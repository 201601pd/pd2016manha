-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27-Jun-2016 às 16:58
-- Versão do servidor: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud_gastronomia`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aula`
--

CREATE TABLE `aula` (
  `id_aula` int(11) NOT NULL,
  `nome_aula` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `aula`
--

INSERT INTO `aula` (`id_aula`, `nome_aula`) VALUES
(1, 'Aula de massas'),
(2, 'Aula de carnes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `aula_receita`
--

CREATE TABLE `aula_receita` (
  `aula_receita_id_aula` int(11) NOT NULL,
  `aula_receita_id_receita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `aula_receita`
--

INSERT INTO `aula_receita` (`aula_receita_id_aula`, `aula_receita_id_receita`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `aula_roteiro`
--

CREATE TABLE `aula_roteiro` (
  `aula_roteiro_id_aula` int(11) NOT NULL,
  `aula_roteiro_id_roteiro` int(11) NOT NULL,
  `data_aula_roteiro` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `aula_roteiro`
--

INSERT INTO `aula_roteiro` (`aula_roteiro_id_aula`, `aula_roteiro_id_roteiro`, `data_aula_roteiro`) VALUES
(1, 1, '2016-10-10'),
(2, 1, '2016-10-11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria_insumo`
--

CREATE TABLE `categoria_insumo` (
  `id_categoria_insumo` int(11) NOT NULL,
  `nome_categoria_insumo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categoria_insumo`
--

INSERT INTO `categoria_insumo` (`id_categoria_insumo`, `nome_categoria_insumo`) VALUES
(1, 'Carnes'),
(2, 'Massas');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria_receita`
--

CREATE TABLE `categoria_receita` (
  `id_categoria_receita` int(11) NOT NULL,
  `nome_categoria_receita` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categoria_receita`
--

INSERT INTO `categoria_receita` (`id_categoria_receita`, `nome_categoria_receita`) VALUES
(1, 'Massas'),
(2, 'Carnes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `curso`
--

CREATE TABLE `curso` (
  `id_curso` int(11) NOT NULL,
  `nome_curso` varchar(50) NOT NULL,
  `descricao` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `curso`
--

INSERT INTO `curso` (`id_curso`, `nome_curso`, `descricao`) VALUES
(1, 'Cozinheiro', 'Curso de cozinheiro');

-- --------------------------------------------------------

--
-- Estrutura da tabela `entrada`
--

CREATE TABLE `entrada` (
  `id_entrada` int(11) NOT NULL,
  `validade` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lote` varchar(50) NOT NULL,
  `data_entrada` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data_entrega` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `qtd_entrada` int(11) NOT NULL,
  `entrada_id_insumo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fornecedor`
--

CREATE TABLE `fornecedor` (
  `id_fornecedor` int(11) NOT NULL,
  `cnpj` varchar(50) NOT NULL,
  `status_fornecedor` tinyint(1) NOT NULL,
  `razao_social` varchar(50) NOT NULL,
  `endereco` varchar(100) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `fornecedor`
--

INSERT INTO `fornecedor` (`id_fornecedor`, `cnpj`, `status_fornecedor`, `razao_social`, `endereco`, `telefone`, `email`) VALUES
(1, '1/1123-45', 1, 'Fornecedor de Carnes', 'Av teste', '(51) 5151-515', 'teste@teste.com'),
(2, '12/3456-54', 1, 'Fornecedor de Massas', 'Av. Teste', '(51) 5155-2', 'gfff@teste.com');

-- --------------------------------------------------------

--
-- Estrutura da tabela `insumo`
--

CREATE TABLE `insumo` (
  `id_insumo` int(11) NOT NULL,
  `nome_insumo` varchar(100) NOT NULL,
  `qtd_minima` int(11) NOT NULL,
  `qtd_atual` int(11) NOT NULL,
  `perecivel` tinyint(1) NOT NULL,
  `insumo_id_medida` int(11) NOT NULL,
  `insumo_id_categoria_insumo` int(11) NOT NULL,
  `insumo_id_fornecedor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `insumo`
--

INSERT INTO `insumo` (`id_insumo`, `nome_insumo`, `qtd_minima`, `qtd_atual`, `perecivel`, `insumo_id_medida`, `insumo_id_categoria_insumo`, `insumo_id_fornecedor`) VALUES
(1, 'Farinha', 100, 500, 1, 1, 2, 2),
(2, 'Guizado', 50, 500, 1, 1, 1, 1),
(3, 'Massa parafuso', 50, 500, 1, 1, 2, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `insumo_receita`
--

CREATE TABLE `insumo_receita` (
  `insumo_receita_id_insumo` int(11) NOT NULL,
  `insumo_receita_id_receita` int(11) NOT NULL,
  `quantidade` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `insumo_receita`
--

INSERT INTO `insumo_receita` (`insumo_receita_id_insumo`, `insumo_receita_id_receita`, `quantidade`) VALUES
(1, 1, 2),
(2, 1, 5),
(2, 2, 10),
(3, 1, 5),
(3, 2, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `medida`
--

CREATE TABLE `medida` (
  `id_medida` int(11) NOT NULL,
  `nome_medida` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `medida`
--

INSERT INTO `medida` (`id_medida`, `nome_medida`) VALUES
(1, 'KG'),
(2, 'Unidade');

-- --------------------------------------------------------

--
-- Estrutura da tabela `receita`
--

CREATE TABLE `receita` (
  `id_receita` int(11) NOT NULL,
  `nome_receita` varchar(50) NOT NULL,
  `modo_preparo` text NOT NULL,
  `observacao` text NOT NULL,
  `receita_id_categoria_receita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `receita`
--

INSERT INTO `receita` (`id_receita`, `nome_receita`, `modo_preparo`, `observacao`, `receita_id_categoria_receita`) VALUES
(1, 'Massa raviolli', 'Misture tudo', 'Massa', 1),
(2, 'Guizado com massa', 'Misture tudo', 'Teste', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `roteiro`
--

CREATE TABLE `roteiro` (
  `id_roteiro` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `roteiro_id_curso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `roteiro`
--

INSERT INTO `roteiro` (`id_roteiro`, `titulo`, `roteiro_id_curso`) VALUES
(1, 'Semana das carnes e massas', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `roteiro_receita`
--

CREATE TABLE `roteiro_receita` (
  `roteiro_receita_id_roteiro` int(11) NOT NULL,
  `roteiro_receita_id_receita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `saida`
--

CREATE TABLE `saida` (
  `id_saida` int(11) NOT NULL,
  `qtd_saida` int(11) NOT NULL,
  `data_saida` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `saida_id_aula` int(11) NOT NULL,
  `saida_id_insumo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `solicitacao`
--

CREATE TABLE `solicitacao` (
  `id_solicitacao` int(11) NOT NULL,
  `data_requisicao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nota` varchar(100) DEFAULT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `url_imagem_nota` text,
  `status_solicitacao` tinyint(1) NOT NULL,
  `solicitacao_id_fornecedor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `solicitacao_insumo`
--

CREATE TABLE `solicitacao_insumo` (
  `solicitacao_insumo_id_insumo` int(11) NOT NULL,
  `solicitacao_insumo_id_solicitacao` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `observacao` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_acesso`
--

CREATE TABLE `tipo_acesso` (
  `id_tipo_acesso` int(11) NOT NULL,
  `nome_tipo_acesso` varchar(50) NOT NULL,
  `descricao` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tipo_acesso`
--

INSERT INTO `tipo_acesso` (`id_tipo_acesso`, `nome_tipo_acesso`, `descricao`) VALUES
(1, 'Administrador', 'Acesso de Administrador');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nome_usuario` varchar(50) NOT NULL,
  `matricula` varchar(20) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `usuario_id_tipo_acesso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nome_usuario`, `matricula`, `senha`, `email`, `usuario_id_tipo_acesso`) VALUES
(1, 'Admin Teste', '123456', 'e10adc3949ba59abbe56e057f20f883e', 'admin@teste.com', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aula`
--
ALTER TABLE `aula`
  ADD PRIMARY KEY (`id_aula`);

--
-- Indexes for table `aula_receita`
--
ALTER TABLE `aula_receita`
  ADD PRIMARY KEY (`aula_receita_id_aula`,`aula_receita_id_receita`),
  ADD KEY `id_aula` (`aula_receita_id_aula`),
  ADD KEY `id_receita` (`aula_receita_id_receita`);

--
-- Indexes for table `aula_roteiro`
--
ALTER TABLE `aula_roteiro`
  ADD PRIMARY KEY (`aula_roteiro_id_aula`,`aula_roteiro_id_roteiro`),
  ADD KEY `id_aula` (`aula_roteiro_id_aula`),
  ADD KEY `id_roteiro` (`aula_roteiro_id_roteiro`);

--
-- Indexes for table `categoria_insumo`
--
ALTER TABLE `categoria_insumo`
  ADD PRIMARY KEY (`id_categoria_insumo`);

--
-- Indexes for table `categoria_receita`
--
ALTER TABLE `categoria_receita`
  ADD PRIMARY KEY (`id_categoria_receita`);

--
-- Indexes for table `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`id_curso`);

--
-- Indexes for table `entrada`
--
ALTER TABLE `entrada`
  ADD PRIMARY KEY (`id_entrada`),
  ADD KEY `id_insumo` (`entrada_id_insumo`);

--
-- Indexes for table `fornecedor`
--
ALTER TABLE `fornecedor`
  ADD PRIMARY KEY (`id_fornecedor`);

--
-- Indexes for table `insumo`
--
ALTER TABLE `insumo`
  ADD PRIMARY KEY (`id_insumo`),
  ADD KEY `id_categoria_insumo` (`insumo_id_categoria_insumo`),
  ADD KEY `id_medida` (`insumo_id_medida`),
  ADD KEY `id_fornecedor` (`insumo_id_fornecedor`) USING BTREE;

--
-- Indexes for table `insumo_receita`
--
ALTER TABLE `insumo_receita`
  ADD PRIMARY KEY (`insumo_receita_id_insumo`,`insumo_receita_id_receita`),
  ADD KEY `id_receita` (`insumo_receita_id_receita`),
  ADD KEY `id_insumo` (`insumo_receita_id_insumo`);

--
-- Indexes for table `medida`
--
ALTER TABLE `medida`
  ADD PRIMARY KEY (`id_medida`);

--
-- Indexes for table `receita`
--
ALTER TABLE `receita`
  ADD PRIMARY KEY (`id_receita`),
  ADD KEY `id_categoria_receita` (`receita_id_categoria_receita`) USING BTREE;

--
-- Indexes for table `roteiro`
--
ALTER TABLE `roteiro`
  ADD PRIMARY KEY (`id_roteiro`),
  ADD KEY `id_curso` (`roteiro_id_curso`);

--
-- Indexes for table `roteiro_receita`
--
ALTER TABLE `roteiro_receita`
  ADD PRIMARY KEY (`roteiro_receita_id_roteiro`,`roteiro_receita_id_receita`),
  ADD KEY `id_roteiro` (`roteiro_receita_id_roteiro`),
  ADD KEY `id_receita` (`roteiro_receita_id_receita`);

--
-- Indexes for table `saida`
--
ALTER TABLE `saida`
  ADD PRIMARY KEY (`id_saida`),
  ADD KEY `id_aula` (`saida_id_aula`),
  ADD KEY `id_insumo` (`saida_id_insumo`);

--
-- Indexes for table `solicitacao`
--
ALTER TABLE `solicitacao`
  ADD PRIMARY KEY (`id_solicitacao`),
  ADD KEY `id_fornecedor` (`solicitacao_id_fornecedor`);

--
-- Indexes for table `solicitacao_insumo`
--
ALTER TABLE `solicitacao_insumo`
  ADD PRIMARY KEY (`solicitacao_insumo_id_insumo`,`solicitacao_insumo_id_solicitacao`),
  ADD KEY `id_insumo` (`solicitacao_insumo_id_insumo`),
  ADD KEY `id_solicitacao` (`solicitacao_insumo_id_solicitacao`);

--
-- Indexes for table `tipo_acesso`
--
ALTER TABLE `tipo_acesso`
  ADD PRIMARY KEY (`id_tipo_acesso`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `matricula` (`matricula`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `id_tipo_acesso` (`usuario_id_tipo_acesso`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aula`
--
ALTER TABLE `aula`
  MODIFY `id_aula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categoria_insumo`
--
ALTER TABLE `categoria_insumo`
  MODIFY `id_categoria_insumo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categoria_receita`
--
ALTER TABLE `categoria_receita`
  MODIFY `id_categoria_receita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `curso`
--
ALTER TABLE `curso`
  MODIFY `id_curso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `entrada`
--
ALTER TABLE `entrada`
  MODIFY `id_entrada` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fornecedor`
--
ALTER TABLE `fornecedor`
  MODIFY `id_fornecedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `insumo`
--
ALTER TABLE `insumo`
  MODIFY `id_insumo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `medida`
--
ALTER TABLE `medida`
  MODIFY `id_medida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `receita`
--
ALTER TABLE `receita`
  MODIFY `id_receita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `roteiro`
--
ALTER TABLE `roteiro`
  MODIFY `id_roteiro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `saida`
--
ALTER TABLE `saida`
  MODIFY `id_saida` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `solicitacao`
--
ALTER TABLE `solicitacao`
  MODIFY `id_solicitacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tipo_acesso`
--
ALTER TABLE `tipo_acesso`
  MODIFY `id_tipo_acesso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `aula_receita`
--
ALTER TABLE `aula_receita`
  ADD CONSTRAINT `aula_constraint` FOREIGN KEY (`aula_receita_id_aula`) REFERENCES `aula` (`id_aula`),
  ADD CONSTRAINT `receita_constraint` FOREIGN KEY (`aula_receita_id_receita`) REFERENCES `receita` (`id_receita`);

--
-- Limitadores para a tabela `aula_roteiro`
--
ALTER TABLE `aula_roteiro`
  ADD CONSTRAINT `aula_constraint2` FOREIGN KEY (`aula_roteiro_id_aula`) REFERENCES `aula` (`id_aula`),
  ADD CONSTRAINT `roteiro_constraint` FOREIGN KEY (`aula_roteiro_id_roteiro`) REFERENCES `roteiro` (`id_roteiro`);

--
-- Limitadores para a tabela `entrada`
--
ALTER TABLE `entrada`
  ADD CONSTRAINT `insumo_constraint` FOREIGN KEY (`entrada_id_insumo`) REFERENCES `insumo` (`id_insumo`);

--
-- Limitadores para a tabela `insumo`
--
ALTER TABLE `insumo`
  ADD CONSTRAINT `categoria_insumo_constraint` FOREIGN KEY (`insumo_id_categoria_insumo`) REFERENCES `categoria_insumo` (`id_categoria_insumo`),
  ADD CONSTRAINT `fornecedor_constraint` FOREIGN KEY (`insumo_id_fornecedor`) REFERENCES `fornecedor` (`id_fornecedor`),
  ADD CONSTRAINT `medida_constraint` FOREIGN KEY (`insumo_id_medida`) REFERENCES `medida` (`id_medida`);

--
-- Limitadores para a tabela `insumo_receita`
--
ALTER TABLE `insumo_receita`
  ADD CONSTRAINT `insumo_constraint2` FOREIGN KEY (`insumo_receita_id_insumo`) REFERENCES `insumo` (`id_insumo`),
  ADD CONSTRAINT `receita_constraint2` FOREIGN KEY (`insumo_receita_id_receita`) REFERENCES `receita` (`id_receita`);

--
-- Limitadores para a tabela `receita`
--
ALTER TABLE `receita`
  ADD CONSTRAINT `categoria_receita_constraint` FOREIGN KEY (`receita_id_categoria_receita`) REFERENCES `categoria_receita` (`id_categoria_receita`);

--
-- Limitadores para a tabela `roteiro`
--
ALTER TABLE `roteiro`
  ADD CONSTRAINT `curso_constraint` FOREIGN KEY (`roteiro_id_curso`) REFERENCES `curso` (`id_curso`);

--
-- Limitadores para a tabela `roteiro_receita`
--
ALTER TABLE `roteiro_receita`
  ADD CONSTRAINT `receita_constraint3` FOREIGN KEY (`roteiro_receita_id_receita`) REFERENCES `receita` (`id_receita`),
  ADD CONSTRAINT `roteiro_constraint2` FOREIGN KEY (`roteiro_receita_id_roteiro`) REFERENCES `roteiro` (`id_roteiro`);

--
-- Limitadores para a tabela `saida`
--
ALTER TABLE `saida`
  ADD CONSTRAINT `aula_constraint3` FOREIGN KEY (`saida_id_aula`) REFERENCES `aula` (`id_aula`),
  ADD CONSTRAINT `insumo_constraint3` FOREIGN KEY (`saida_id_insumo`) REFERENCES `insumo` (`id_insumo`);

--
-- Limitadores para a tabela `solicitacao`
--
ALTER TABLE `solicitacao`
  ADD CONSTRAINT `fornecedor_constraint2` FOREIGN KEY (`solicitacao_id_fornecedor`) REFERENCES `fornecedor` (`id_fornecedor`);

--
-- Limitadores para a tabela `solicitacao_insumo`
--
ALTER TABLE `solicitacao_insumo`
  ADD CONSTRAINT `insumo_constraint4` FOREIGN KEY (`solicitacao_insumo_id_insumo`) REFERENCES `insumo` (`id_insumo`),
  ADD CONSTRAINT `solicitacao_constraint` FOREIGN KEY (`solicitacao_insumo_id_solicitacao`) REFERENCES `solicitacao` (`id_solicitacao`);

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `tipo_acesso_constraint` FOREIGN KEY (`usuario_id_tipo_acesso`) REFERENCES `tipo_acesso` (`id_tipo_acesso`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
