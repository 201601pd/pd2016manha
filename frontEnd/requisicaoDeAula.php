<?php require_once("header.php"); ?>

<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">

        <div align="left" style="margin-left: 10px">
			<div class="form-group">
				<form action="#" align="center">
					
					<div class="form-group label-floating" style="width: 40%">
							<select id="select111" class="form-control">
								<option>Cursos </option>
								<option>Curso 1</option>
								<option>Curso 2</option>
								<option>Curso 3</option>
								<option>Curso 4</option>
							</select>
					</div>

					<div class="form-group label-floating" style="width: 40%">
						<label for="i5" class="control-label">Turno do Curso...</label>
						<input type="text" class="form-control" id="i5">      
					</div>

					<div class="form-group label-floating" style="width: 40%">
						<label for="i5" class="control-label">Data de Solicitação...</label>
						<input type="date" class="form-control" id="i5">      
					</div>
					
					<div class="form-group label-floating" style="width: 40%">
						<label for="i5" class="control-label">Data de Entrega...</label>
						<input type="date" class="form-control" id="i5">      
					</div>
				</form>
			</div>
		</div>

        <div class="tabelarequisicaodeaula" align="left">
            <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
                <thead>
                    <tr>
                        <th>Especificação do Insumo</th>
                        <th>Estoque</th>
                        <th>Quantidade</th>
                        <th>Unidade</th>
                        <th>Observação</th>
                        <th>Excluir</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Tomate</td>
                        <td>7</td>
                        <td class="mdl-data-table__cell--non-numeric"><input type="number" name="quantidade" value="10" min="1"></td>
                        <td>Kg</td>
                        <td><input type="text" name="observacao"></td>
                        <td><a href="#" class="mdl-button mdl-js-button mdl-button--raised  mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast">X</a></td>
                    </tr>
                    <tr>
                        <td>Cebola</td>
                        <td>2</td>
                        <td class="mdl-data-table__cell--non-numeric"><input type="number" name="quantidade" value="10" min="1"></td>
                        <td>Kg</td>
                        <td><input type="text" name="observacao"></td>
                        <td><a href="#" class="mdl-button mdl-js-button mdl-button--raised  mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast">X</a></td>
                    </tr>
                    <tr>
                        <td>Laranja</td>
                        <td>10</td>
                        <td class="mdl-data-table__cell--non-numeric"><input type="number" name="quantidade" value="10" min="1"></td>
                        <td>Kg</td>
                        <td><input type="text" name="observacao"></td>
                        <td><a href="#" class="mdl-button mdl-js-button mdl-button--raised  mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast">X</a></td>
                    </tr>
                    <tr>
                        <td>Batata</td>
                        <td>12</td>
                        <td class="mdl-data-table__cell--non-numeric"><input type="number" name="quantidade" value="10" min="1"></td>
                        <td>Kg</td>
                        <td><input type="text" name="observacao"></td>
                        <td><a href="#" class="mdl-button mdl-js-button mdl-button--raised  mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast">X</a></td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>
</main>
<br>

<?php require_once("footer.php"); ?>