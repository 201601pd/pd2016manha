<?php require_once("header.php"); ?>

<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">

        <section class="mdl-grid--no-spacing mdl-layout mdl-shadow--2dp" style="width: 60%">
            <h6 style="margin-left: 5px">Selecione o curso desejado:</h6>
            <select class="select" placeholder="Curso" style="width: 25%; margin-left: 10px">
                <option>Gastronomia - Manhã</option>
                <option>Gastronomia - Noite</option>
            </select>

            <h6 style="margin-left: 5px">Selecione o roteiro:</h6>
            <select class="select" placeholder="Roteiro" style="width: 25%; margin-left: 10px">
                <option>Roteiro 1</option>
                <option>Roteiro 2</option>
                <option>Roteiro 3</option>
                <option>Roteiro 4</option>
            </select>
            <hr/>
            <table class="tabela" style="width:45%; margin-left: 10px; line-height: 3">
                <tr>
                    <td>   
                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-1">
                            <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input">
                            <span class="mdl-checkbox__label">Aula 1</span>
                        </label>
                    </td>
                    <td> 
                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-2">
                            <input type="checkbox" id="checkbox-2" class="mdl-checkbox__input">
                            <span class="mdl-checkbox__label">Aula 2</span>
                        </label>
                    </td>
                    <td> 
                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-3">
                            <input type="checkbox" id="checkbox-3" class="mdl-checkbox__input" >
                            <span class="mdl-checkbox__label">Aula 3</span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td> 
                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-4">
                            <input type="checkbox" id="checkbox-4" class="mdl-checkbox__input">
                            <span class="mdl-checkbox__label">Aula 4</span>
                        </label>
                    </td>
                    <td> 
                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-5">
                            <input type="checkbox" id="checkbox-5" class="mdl-checkbox__input">
                            <span class="mdl-checkbox__label">Aula 5</span>
                        </label>
                    </td>
                </tr>
            </table>


            <a href="#" id="view-source" class="mdl-button mdl-js-button mdl-button--raised  mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast" style="margin: 30px 0px 15px 15px">Gerar Requisição</a>
        </section>

    </div>

</main>

<?php require_once("footer.php"); ?>
