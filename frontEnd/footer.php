<footer class="mdl-mega-footer">
    <div class="mdl-mega-footer--middle-section">
        <div class="mdl-mega-footer--drop-down-section">
            <input class="mdl-mega-footer--heading-checkbox" type="checkbox" checked>
            <h1 class="mdl-mega-footer--heading">Localização</h1>
            <ul class="mdl-mega-footer--link-list">
                <li>Rua Coronel Genuíno, 130</li>
                <li>Centro Histórico - Porto Alegre/RS</li>
                <li>CEP: 90010-350</li>
                <li>Tel: (51) 3022-1044</li>
            </ul>
        </div>
        <div class="mdl-mega-footer--drop-down-section col-md-push-8">
            <input class="mdl-mega-footer--heading-checkbox" type="checkbox" checked>
            <h1 class="mdl-mega-footer--heading">Redes Sociais</h1>
            <ul class="mdl-mega-footer--link-list">
                <li style="margin-left: -8px;"><a href="http://www.facebook.com/senacrsoficial"><img src="../frontEnd/images/icone_facebook.png"/></a>
                    <a href="http://www.twitter.com/senacrs"><img src="../frontEnd/images/icone_twitter.png"/></a></li>

            </ul>
        </div>

</footer>

</div>

<script src="js/jquery-2.1.1.min.js"></script>
<script src="js/material.min.js"></script>
<script src="js/ripples.min.js"></script>
<script src="js/jquery.dropdown.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../frontEnd/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="../frontEnd/js/app.min.js"></script> 
<script>
    $.material.init();
    $(document).ready(function () {
        $(".select").dropdown({"optionClass": "withripple"});
    });
</script>

  <!-- <script src="https://code.getmdl.io/1.1.3/material.min.js"></script> -->
</body>
</html>