﻿-- 1)Relatório sobre Desperdícios *Há de ser conferido, alterar lote para UNIQUE, adicionar um auto-increment.

SELECT nome_insumo, qtd_atual, data_entrada, validade, lote FROM insumo INNER JOIN entrada ON 
(id_insumo = entrada_id_insumo) WHERE (validade < NOW())

-- 2)Relatório de Produtos mais utilizados

SELECT nome_insumo, SUM(qtd_saida) AS qtd_total FROM insumo INNER JOIN saida ON (saida_id_insumo = id_insumo)
GROUP BY id_insumo ORDER BY qtd_total DESC

-- 3)Solicitações Realizadas por mês (filtradas por mês)

SELECT * FROM solicitacao WHERE MONTH(data_requisicao) = 1 AND YEAR(data_requisicao) = 2016

-- 4)Total gasto com fornecedor

SELECT cnpj, status_fornecedor, razao_social, endereco, SUM(valor) as valor_total FROM fornecedor INNER JOIN solicitacao ON 
(solicitacao_id_fornecedor = id_fornecedor) GROUP BY id_fornecedor