DELIMITER $$;
CREATE TRIGGER `procedure_entrada_insert` AFTER INSERT ON `entrada`
 FOR EACH ROW BEGIN
   UPDATE insumo 
   SET qtd_atual = qtd_atual + NEW.qtd_entrada 
   WHERE id_insumo  = NEW.entrada_id_insumo;
END$$;
DELIMITER ;

DELIMITER $$;
CREATE TRIGGER `procedure_entrada_update` AFTER UPDATE ON `entrada`
 FOR EACH ROW BEGIN
   UPDATE insumo 
   SET qtd_atual = qtd_atual - OLD.qtd_entrada + NEW.qtd_entrada  
   WHERE id_insumo  = NEW.entrada_id_insumo;
END$$;
DELIMITER ;

DELIMITER $$;
CREATE TRIGGER `procedure_entrada_delete` AFTER DELETE ON `entrada`
 FOR EACH ROW BEGIN
   UPDATE insumo 
   SET qtd_atual = qtd_atual - OLD.qtd_entrada 
   WHERE OLD.entrada_id_insumo = id_insumo;
END$$;
DELIMITER ;

DELIMITER $$;
CREATE TRIGGER `procedure_saida_insert` AFTER INSERT ON `saida`
 FOR EACH ROW BEGIN
   UPDATE insumo 
   SET qtd_atual = qtd_atual - NEW.qtd_saida 
   WHERE NEW.saida_id_insumo = id_insumo;
END$$;
DELIMITER ;

DELIMITER $$;
CREATE TRIGGER procedure_saida_update AFTER UPDATE ON saida
 FOR EACH ROW BEGIN
   UPDATE insumo 
   SET qtd_atual = qtd_atual + OLD.qtd_saida - NEW.qtd_saida
   WHERE NEW.saida_id_insumo = id_insumo;
END$$;
DELIMITER ;

DELIMITER $$;
CREATE TRIGGER procedure_saida_delete AFTER DELETE ON saida
 FOR EACH ROW BEGIN
   UPDATE insumo 
   SET qtd_atual = qtd_atual + OLD.qtd_saida
   WHERE OLD.saida_id_insumo = id_insumo;
END$$;
DELIMITER ;




