-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 02-Jul-2016 às 05:55
-- Versão do servidor: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud_gastronomia`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aula`
--

CREATE TABLE `aula` (
  `id_aula` int(11) NOT NULL,
  `nome_aula` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `aula`
--

INSERT INTO `aula` (`id_aula`, `nome_aula`) VALUES
(1, 'Controle e organização de estoque de cozinha'),
(2, 'Organização do ambiente de trabalho para elaboração das produções culinárias '),
(3, 'Pré-preparação dos ingredientes para elaboração das produções culinárias'),
(4, 'Elaboração e preparação de cardápios');

-- --------------------------------------------------------

--
-- Estrutura da tabela `aula_receita`
--

CREATE TABLE `aula_receita` (
  `aula_receita_id_aula` int(11) NOT NULL,
  `aula_receita_id_receita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `aula_receita`
--

INSERT INTO `aula_receita` (`aula_receita_id_aula`, `aula_receita_id_receita`) VALUES
(1, 3),
(4, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `aula_roteiro`
--

CREATE TABLE `aula_roteiro` (
  `aula_roteiro_id_aula` int(11) NOT NULL,
  `aula_roteiro_id_roteiro` int(11) NOT NULL,
  `data_aula_roteiro` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `aula_roteiro`
--

INSERT INTO `aula_roteiro` (`aula_roteiro_id_aula`, `aula_roteiro_id_roteiro`, `data_aula_roteiro`) VALUES
(1, 1, '2016-07-05'),
(4, 1, '2016-07-04');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria_insumo`
--

CREATE TABLE `categoria_insumo` (
  `id_categoria_insumo` int(11) NOT NULL,
  `nome_categoria_insumo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categoria_insumo`
--

INSERT INTO `categoria_insumo` (`id_categoria_insumo`, `nome_categoria_insumo`) VALUES
(1, 'Massas'),
(2, 'Carnes'),
(3, 'Legumes'),
(4, 'Verduras'),
(5, 'Temperos'),
(6, 'Frutas');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria_receita`
--

CREATE TABLE `categoria_receita` (
  `id_categoria_receita` int(11) NOT NULL,
  `nome_categoria_receita` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categoria_receita`
--

INSERT INTO `categoria_receita` (`id_categoria_receita`, `nome_categoria_receita`) VALUES
(1, 'Molhos'),
(2, 'Risotos'),
(3, 'Canapés');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('60c862d4732bb748121c16edadd5a5e8d37bf8d8', '::1', 1466427325, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363432373332353b);

-- --------------------------------------------------------

--
-- Estrutura da tabela `curso`
--

CREATE TABLE `curso` (
  `id_curso` int(11) NOT NULL,
  `nome_curso` varchar(50) NOT NULL,
  `descricao` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `curso`
--

INSERT INTO `curso` (`id_curso`, `nome_curso`, `descricao`) VALUES
(1, 'Curso de Confeiteiro', 'O curso visa a propiciar aos participantes o domínio das técnicas de confeitaria, agregando conhecimentos e competências relacionadas às exigências do mercado.'),
(2, 'Curso de Cozinheiro', 'O Cozinheiro é o profissional que tem como atribuição a preparação e a apresentação de produções culinárias que compõem os cardápios de estabelecimentos alimentícios.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `entrada`
--

CREATE TABLE `entrada` (
  `id_entrada` int(11) NOT NULL,
  `validade` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lote` varchar(50) NOT NULL,
  `data_entrada` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data_entrega` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `qtd_entrada` int(11) NOT NULL,
  `entrada_id_insumo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fornecedor`
--

CREATE TABLE `fornecedor` (
  `id_fornecedor` int(11) NOT NULL,
  `cnpj` varchar(50) NOT NULL,
  `status_fornecedor` tinyint(1) NOT NULL,
  `razao_social` varchar(50) NOT NULL,
  `endereco` varchar(100) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `fornecedor`
--

INSERT INTO `fornecedor` (`id_fornecedor`, `cnpj`, `status_fornecedor`, `razao_social`, `endereco`, `telefone`, `email`) VALUES
(1, '99.999.888/9999-97', 1, 'Fornecedor de frutas', 'Rua Coronel Genuíno, 600', '(51) 8827-7338', 'fornecedor@frutas.com.br'),
(2, '02.883.772/7723-33', 1, 'Fornecedor de carnes', 'Rua Mariante, 317', '(51) 3779-1403', 'fornecedor@carnes.com.br'),
(3, '92.877.288/8823-32', 1, 'Fornecedor de temperos', 'Rua General Lima e Silva, 12', '(51) 9927-7732', 'fornecedor@temperos.com.br'),
(4, '98.223.232/1323-23', 1, 'Fornecedor de insumos diversos', 'Rua São Luis, 1122', '(51) 8827-7722', 'fornecedor@insumo.com.br');

-- --------------------------------------------------------

--
-- Estrutura da tabela `insumo`
--

CREATE TABLE `insumo` (
  `id_insumo` int(11) NOT NULL,
  `nome_insumo` varchar(100) NOT NULL,
  `qtd_minima` int(11) NOT NULL,
  `qtd_atual` int(11) NOT NULL,
  `perecivel` tinyint(1) NOT NULL,
  `insumo_id_medida` int(11) NOT NULL,
  `insumo_id_categoria_insumo` int(11) NOT NULL,
  `insumo_id_fornecedor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `insumo`
--

INSERT INTO `insumo` (`id_insumo`, `nome_insumo`, `qtd_minima`, `qtd_atual`, `perecivel`, `insumo_id_medida`, `insumo_id_categoria_insumo`, `insumo_id_fornecedor`) VALUES
(1, 'Carne moída', 5, 3, 1, 1, 2, 2),
(2, 'Massa penne', 10, 5, 1, 1, 1, 4),
(3, 'Sal', 10, 1, 0, 1, 5, 3),
(4, 'Molho de tomate', 10, 3, 1, 2, 5, 3),
(5, 'Cebola', 10, 4, 1, 3, 3, 4),
(6, 'Tomate', 10, 3, 1, 3, 3, 4),
(7, 'Iscas de carne', 10, 5, 1, 1, 2, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `insumo_receita`
--

CREATE TABLE `insumo_receita` (
  `insumo_receita_id_insumo` int(11) NOT NULL,
  `insumo_receita_id_receita` int(11) NOT NULL,
  `quantidade` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `insumo_receita`
--

INSERT INTO `insumo_receita` (`insumo_receita_id_insumo`, `insumo_receita_id_receita`, `quantidade`) VALUES
(1, 3, 0.3),
(2, 1, 200),
(4, 1, 0.5),
(4, 3, 0.4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `medida`
--

CREATE TABLE `medida` (
  `id_medida` int(11) NOT NULL,
  `nome_medida` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `medida`
--

INSERT INTO `medida` (`id_medida`, `nome_medida`) VALUES
(1, 'kg'),
(2, 'litros'),
(3, 'unidades'),
(4, 'colheres (sopa)'),
(5, 'colheres (chá)');

-- --------------------------------------------------------

--
-- Estrutura da tabela `receita`
--

CREATE TABLE `receita` (
  `id_receita` int(11) NOT NULL,
  `nome_receita` varchar(50) NOT NULL,
  `modo_preparo` text NOT NULL,
  `observacao` text NOT NULL,
  `receita_id_categoria_receita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `receita`
--

INSERT INTO `receita` (`id_receita`, `nome_receita`, `modo_preparo`, `observacao`, `receita_id_categoria_receita`) VALUES
(1, 'Massa ao sugo', 'Pique os tomates pelados, a cebola, as folhas de manjericão e o alho.  2- Em uma panela coloque um fio de azeite e refogue a cebola com o alho, adicione o tomate pelado, o açúcar e deixe apurar alguns instantes. Tempere com sal e pimenta do reino e manjericão. Desligue o fogo e reserve.  3- Em uma panela com água fervente e sal, cozinhe o espaguete até que ele esteja “al dente”, não deixe passar do ponto.  4- Escorra a massa muito bem e despeje na panela com o molho, misture rapidamente com o fogo ligado e sirva a seguir salpicando parmesão fresco.', 'Preparo rápido', 1),
(3, 'Guisado ao molho', 'Adicione guisado e molho', 'Rápido...', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `roteiro`
--

CREATE TABLE `roteiro` (
  `id_roteiro` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `roteiro_id_curso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `roteiro`
--

INSERT INTO `roteiro` (`id_roteiro`, `titulo`, `roteiro_id_curso`) VALUES
(1, 'Semana 04/07 a 08/07', 2),
(2, 'Semana 11/07 a 15/07', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `roteiro_receita`
--

CREATE TABLE `roteiro_receita` (
  `roteiro_receita_id_roteiro` int(11) NOT NULL,
  `roteiro_receita_id_receita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `saida`
--

CREATE TABLE `saida` (
  `id_saida` int(11) NOT NULL,
  `qtd_saida` int(11) NOT NULL,
  `data_saida` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `saida_id_aula` int(11) NOT NULL,
  `saida_id_insumo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `solicitacao`
--

CREATE TABLE `solicitacao` (
  `id_solicitacao` int(11) NOT NULL,
  `data_requisicao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nota` varchar(100) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `url_imagem_nota` text NOT NULL,
  `solicitacao_id_fornecedor` int(11) NOT NULL,
  `status_solicitacao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `solicitacao`
--

INSERT INTO `solicitacao` (`id_solicitacao`, `data_requisicao`, `nota`, `tipo`, `valor`, `url_imagem_nota`, `solicitacao_id_fornecedor`, `status_solicitacao`) VALUES
(31, '2016-07-02 03:50:40', '', '', '0.00', '', 2, 1),
(32, '2016-07-02 03:50:40', '', '', '0.00', '', 3, 1),
(33, '2016-07-02 03:50:40', '', '', '0.00', '', 4, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `solicitacao_insumo`
--

CREATE TABLE `solicitacao_insumo` (
  `solicitacao_insumo_id_insumo` int(11) NOT NULL,
  `solicitacao_insumo_id_solicitacao` int(11) NOT NULL,
  `quantidade` double NOT NULL,
  `observacao` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `solicitacao_insumo`
--

INSERT INTO `solicitacao_insumo` (`solicitacao_insumo_id_insumo`, `solicitacao_insumo_id_solicitacao`, `quantidade`, `observacao`) VALUES
(1, 31, 0.3, 'De primeira.'),
(2, 33, 200, ''),
(4, 32, 0.9, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_acesso`
--

CREATE TABLE `tipo_acesso` (
  `id_tipo_acesso` int(11) NOT NULL,
  `nome_tipo_acesso` varchar(50) NOT NULL,
  `descricao` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tipo_acesso`
--

INSERT INTO `tipo_acesso` (`id_tipo_acesso`, `nome_tipo_acesso`, `descricao`) VALUES
(1, 'Coordenador', 'Controle total de todas as aulas, roteiros, receita e do estoque.'),
(2, 'Auxiliar', 'Auxilia no controle do estoque.'),
(3, 'Professor', 'Escolhe os dias das aulas, os roteiros necessários e a receita que será utilizada.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nome_usuario` varchar(50) NOT NULL,
  `matricula` varchar(20) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `usuario_id_tipo_acesso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nome_usuario`, `matricula`, `senha`, `email`, `usuario_id_tipo_acesso`) VALUES
(4, '001', '001', 'a821423c737bbee20ce66df14660250c', 'alinedecampos@gmail.com', 1),
(5, 'Coordenador', 'coordenador', '81dc9bdb52d04dc20036dbd8313ed055', 'coordenador@senacrs.com.br', 1),
(6, 'Secretaria', 'secretaria', '81dc9bdb52d04dc20036dbd8313ed055', 'secretaria@senacrs.com.br', 2),
(7, 'Professor', 'professor', '81dc9bdb52d04dc20036dbd8313ed055', 'professor@senacrs.com.br', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aula`
--
ALTER TABLE `aula`
  ADD PRIMARY KEY (`id_aula`);

--
-- Indexes for table `aula_receita`
--
ALTER TABLE `aula_receita`
  ADD PRIMARY KEY (`aula_receita_id_aula`,`aula_receita_id_receita`),
  ADD KEY `id_aula` (`aula_receita_id_aula`),
  ADD KEY `id_receita` (`aula_receita_id_receita`);

--
-- Indexes for table `aula_roteiro`
--
ALTER TABLE `aula_roteiro`
  ADD PRIMARY KEY (`aula_roteiro_id_aula`,`aula_roteiro_id_roteiro`),
  ADD KEY `id_aula` (`aula_roteiro_id_aula`),
  ADD KEY `id_roteiro` (`aula_roteiro_id_roteiro`);

--
-- Indexes for table `categoria_insumo`
--
ALTER TABLE `categoria_insumo`
  ADD PRIMARY KEY (`id_categoria_insumo`);

--
-- Indexes for table `categoria_receita`
--
ALTER TABLE `categoria_receita`
  ADD PRIMARY KEY (`id_categoria_receita`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`id_curso`);

--
-- Indexes for table `entrada`
--
ALTER TABLE `entrada`
  ADD PRIMARY KEY (`id_entrada`),
  ADD KEY `id_insumo` (`entrada_id_insumo`);

--
-- Indexes for table `fornecedor`
--
ALTER TABLE `fornecedor`
  ADD PRIMARY KEY (`id_fornecedor`);

--
-- Indexes for table `insumo`
--
ALTER TABLE `insumo`
  ADD PRIMARY KEY (`id_insumo`),
  ADD KEY `id_categoria_insumo` (`insumo_id_categoria_insumo`),
  ADD KEY `id_medida` (`insumo_id_medida`),
  ADD KEY `id_fornecedor` (`insumo_id_fornecedor`) USING BTREE;

--
-- Indexes for table `insumo_receita`
--
ALTER TABLE `insumo_receita`
  ADD PRIMARY KEY (`insumo_receita_id_insumo`,`insumo_receita_id_receita`),
  ADD KEY `id_receita` (`insumo_receita_id_receita`),
  ADD KEY `id_insumo` (`insumo_receita_id_insumo`);

--
-- Indexes for table `medida`
--
ALTER TABLE `medida`
  ADD PRIMARY KEY (`id_medida`);

--
-- Indexes for table `receita`
--
ALTER TABLE `receita`
  ADD PRIMARY KEY (`id_receita`),
  ADD KEY `id_categoria_receita` (`receita_id_categoria_receita`) USING BTREE;

--
-- Indexes for table `roteiro`
--
ALTER TABLE `roteiro`
  ADD PRIMARY KEY (`id_roteiro`),
  ADD KEY `id_curso` (`roteiro_id_curso`);

--
-- Indexes for table `roteiro_receita`
--
ALTER TABLE `roteiro_receita`
  ADD PRIMARY KEY (`roteiro_receita_id_roteiro`,`roteiro_receita_id_receita`),
  ADD KEY `id_roteiro` (`roteiro_receita_id_roteiro`),
  ADD KEY `id_receita` (`roteiro_receita_id_receita`);

--
-- Indexes for table `saida`
--
ALTER TABLE `saida`
  ADD PRIMARY KEY (`id_saida`),
  ADD KEY `id_aula` (`saida_id_aula`),
  ADD KEY `id_insumo` (`saida_id_insumo`);

--
-- Indexes for table `solicitacao`
--
ALTER TABLE `solicitacao`
  ADD PRIMARY KEY (`id_solicitacao`),
  ADD KEY `id_fornecedor` (`solicitacao_id_fornecedor`);

--
-- Indexes for table `solicitacao_insumo`
--
ALTER TABLE `solicitacao_insumo`
  ADD PRIMARY KEY (`solicitacao_insumo_id_insumo`,`solicitacao_insumo_id_solicitacao`),
  ADD KEY `id_insumo` (`solicitacao_insumo_id_insumo`),
  ADD KEY `id_solicitacao` (`solicitacao_insumo_id_solicitacao`);

--
-- Indexes for table `tipo_acesso`
--
ALTER TABLE `tipo_acesso`
  ADD PRIMARY KEY (`id_tipo_acesso`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `matricula` (`matricula`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `id_tipo_acesso` (`usuario_id_tipo_acesso`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aula`
--
ALTER TABLE `aula`
  MODIFY `id_aula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `categoria_insumo`
--
ALTER TABLE `categoria_insumo`
  MODIFY `id_categoria_insumo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `categoria_receita`
--
ALTER TABLE `categoria_receita`
  MODIFY `id_categoria_receita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `curso`
--
ALTER TABLE `curso`
  MODIFY `id_curso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `entrada`
--
ALTER TABLE `entrada`
  MODIFY `id_entrada` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fornecedor`
--
ALTER TABLE `fornecedor`
  MODIFY `id_fornecedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `insumo`
--
ALTER TABLE `insumo`
  MODIFY `id_insumo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `medida`
--
ALTER TABLE `medida`
  MODIFY `id_medida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `receita`
--
ALTER TABLE `receita`
  MODIFY `id_receita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `roteiro`
--
ALTER TABLE `roteiro`
  MODIFY `id_roteiro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `saida`
--
ALTER TABLE `saida`
  MODIFY `id_saida` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `solicitacao`
--
ALTER TABLE `solicitacao`
  MODIFY `id_solicitacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `tipo_acesso`
--
ALTER TABLE `tipo_acesso`
  MODIFY `id_tipo_acesso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `aula_receita`
--
ALTER TABLE `aula_receita`
  ADD CONSTRAINT `aula_constraint` FOREIGN KEY (`aula_receita_id_aula`) REFERENCES `aula` (`id_aula`),
  ADD CONSTRAINT `receita_constraint` FOREIGN KEY (`aula_receita_id_receita`) REFERENCES `receita` (`id_receita`);

--
-- Limitadores para a tabela `aula_roteiro`
--
ALTER TABLE `aula_roteiro`
  ADD CONSTRAINT `aula_constraint2` FOREIGN KEY (`aula_roteiro_id_aula`) REFERENCES `aula` (`id_aula`),
  ADD CONSTRAINT `roteiro_constraint` FOREIGN KEY (`aula_roteiro_id_roteiro`) REFERENCES `roteiro` (`id_roteiro`);

--
-- Limitadores para a tabela `entrada`
--
ALTER TABLE `entrada`
  ADD CONSTRAINT `insumo_constraint` FOREIGN KEY (`entrada_id_insumo`) REFERENCES `insumo` (`id_insumo`);

--
-- Limitadores para a tabela `insumo`
--
ALTER TABLE `insumo`
  ADD CONSTRAINT `categoria_insumo_constraint` FOREIGN KEY (`insumo_id_categoria_insumo`) REFERENCES `categoria_insumo` (`id_categoria_insumo`),
  ADD CONSTRAINT `fornecedor_constraint` FOREIGN KEY (`insumo_id_fornecedor`) REFERENCES `fornecedor` (`id_fornecedor`),
  ADD CONSTRAINT `medida_constraint` FOREIGN KEY (`insumo_id_medida`) REFERENCES `medida` (`id_medida`);

--
-- Limitadores para a tabela `insumo_receita`
--
ALTER TABLE `insumo_receita`
  ADD CONSTRAINT `insumo_constraint2` FOREIGN KEY (`insumo_receita_id_insumo`) REFERENCES `insumo` (`id_insumo`),
  ADD CONSTRAINT `receita_constraint2` FOREIGN KEY (`insumo_receita_id_receita`) REFERENCES `receita` (`id_receita`);

--
-- Limitadores para a tabela `receita`
--
ALTER TABLE `receita`
  ADD CONSTRAINT `categoria_receita_constraint` FOREIGN KEY (`receita_id_categoria_receita`) REFERENCES `categoria_receita` (`id_categoria_receita`);

--
-- Limitadores para a tabela `roteiro`
--
ALTER TABLE `roteiro`
  ADD CONSTRAINT `curso_constraint` FOREIGN KEY (`roteiro_id_curso`) REFERENCES `curso` (`id_curso`);

--
-- Limitadores para a tabela `roteiro_receita`
--
ALTER TABLE `roteiro_receita`
  ADD CONSTRAINT `receita_constraint3` FOREIGN KEY (`roteiro_receita_id_receita`) REFERENCES `receita` (`id_receita`),
  ADD CONSTRAINT `roteiro_constraint2` FOREIGN KEY (`roteiro_receita_id_roteiro`) REFERENCES `roteiro` (`id_roteiro`);

--
-- Limitadores para a tabela `saida`
--
ALTER TABLE `saida`
  ADD CONSTRAINT `aula_constraint3` FOREIGN KEY (`saida_id_aula`) REFERENCES `aula` (`id_aula`),
  ADD CONSTRAINT `insumo_constraint3` FOREIGN KEY (`saida_id_insumo`) REFERENCES `insumo` (`id_insumo`);

--
-- Limitadores para a tabela `solicitacao`
--
ALTER TABLE `solicitacao`
  ADD CONSTRAINT `fornecedor_constraint2` FOREIGN KEY (`solicitacao_id_fornecedor`) REFERENCES `fornecedor` (`id_fornecedor`);

--
-- Limitadores para a tabela `solicitacao_insumo`
--
ALTER TABLE `solicitacao_insumo`
  ADD CONSTRAINT `insumo_constraint4` FOREIGN KEY (`solicitacao_insumo_id_insumo`) REFERENCES `insumo` (`id_insumo`),
  ADD CONSTRAINT `solicitacao_constraint` FOREIGN KEY (`solicitacao_insumo_id_solicitacao`) REFERENCES `solicitacao` (`id_solicitacao`);

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `tipo_acesso_constraint` FOREIGN KEY (`usuario_id_tipo_acesso`) REFERENCES `tipo_acesso` (`id_tipo_acesso`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
