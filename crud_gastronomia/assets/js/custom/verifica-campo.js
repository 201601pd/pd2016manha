$(function () {
    var inst = $('[data-remodal-id=modal]').remodal();
    var path = window.location.href;
    var originalPath = path;

    if (path.search("editar") != -1) {
        path = path.substring(0, path.substring(0, path.lastIndexOf("/")).lastIndexOf("/") + 1);
        path = path.substring(0, path.length - 1);
    }
    //Função que escuta qualquer mudança no input em questão.
    $(".campo-validado").on('input change', function () {
        //Vars usadas: 
        //Botão submit para poder desativá-lo se necessário;
        //Campo com o id, para poder verificar na edição;
        //Botão do próprio input, para poder visualizar melhor no código.
        var $submit = $('input[type="submit"]');
        var $idValidado = $('#id-validado').val();
        var $campoValidadoInput = $(this);

        //Método que aplica uma função em cima de um json com todos os valores já cadastrados.
        $.getJSON(path + '/getCamposCadastrados', function (data) {
            //Variável para verificar depois se em algum momento fora encontrado um valor igual ao digitado.
            var $exists = false;
            //Percorre o json em busca de um input igual ao digitado.
            //Caso encontre (case insensitive), o $exists é settado para true para comparações posteriores.
            $.each(data, function (i, obj) {
                if (obj.campo_validado.toUpperCase() == ($campoValidadoInput.val()).toUpperCase()) {
                    $exists = true;
                }
            });

            //Pega a url da página e verifica se contém a string "editar".
            //Isso fará com que outro método de leitura de json seja executado
            //em cima do anterior, para poder "liberar" a edição do input já cadastrado.
            //Caso o id for igual ao do atualmente editado, ele deixa gravar este input mesmo
            //sendo igual ao encontrado no json, settando o exists novamente para false.
            if (originalPath.search("editar") != -1) {
                $.getJSON(path + '/getCampoEditado/' + $idValidado, function (data) {
                    $.each(data, function (i, obj) {
                        if ((obj.id == $idValidado) &&
                                ((obj.campo_validado.toUpperCase()) == (($campoValidadoInput.val()).toUpperCase()))) {
                            $exists = false;
                        }
                    });

                    if ($exists) {
                        $submit.prop('disabled', true);
                        //Dá um feedback com um modal do remodal (disposto na respectiva página chamada)
                        inst.open();
                    } else {
                        $submit.prop('disabled', false);
                    }
                });
                //Caso não seja o editar, ele já pula para a verificação de desabilitação do botão.
            } else {
                if ($exists) {
                    $submit.prop('disabled', true);
                    //Dá um feedback com um modal do remodal (disposto na respectiva página chamada)
                    inst.open();
                } else {
                    $submit.prop('disabled', false);
                }
            }
        });
    });
});

