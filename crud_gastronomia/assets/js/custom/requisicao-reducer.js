function resetaCombo(el) {
    $("select[name='" + el + "']").empty();
    var option = document.createElement('option');
    $(option).attr({value: ''});
    $(option).append('Escolha');
    $("select[name='" + el + "']").append(option);
}

function resetaTabelaInsumos() {
    $('#insumos-table tbody tr').remove();
}

function resetaTabelaAulas() {
    $('#aulas-table tbody tr').remove();
}

function resetaDivs(ids) {
    for (var i = 0; i < ids.length; i++) {
        $(ids[i]).empty();
    }
}

$(function () {
    var path = window.location.href;

    $("select[name=curso]").change(function () {
        curso = $(this).val();

        //Reseta combos/tabelas e campos ocultos
        //quando é alterado para não haver nenhuma inconsistência.
        resetaCombo('roteiro');
        resetaTabelaAulas();
        resetaTabelaInsumos();
        resetaDivs(['#id-curso', '#id-roteiro', '#id-aulas', '#id-insumos']);

        $('<input>').attr({
            type: 'hidden',
            value: curso,
            name: 'curso'
        }).appendTo('#id-curso');
        $.getJSON(path + '/getRoteiros/' + curso, function (data) {
            var option = new Array();

            $.each(data, function (i, obj) {
                option[i] = document.createElement('option');
                $(option[i]).attr({value: obj.id_roteiro});
                $(option[i]).append(obj.titulo);

                $("select[name='roteiro']").append(option[i]);
            });
        });
    });

    $("select[name=roteiro]").change(function () {
        roteiro = $(this).val();

        //Reseta combos/tabelas e campos ocultos
        //quando é alterado para não haver nenhuma inconsistência.
        resetaTabelaAulas();
        resetaTabelaInsumos();
        resetaDivs(['#id-roteiro', '#id-aulas', '#id-insumos']);

        $('<input>').attr({
            type: 'hidden',
            value: roteiro,
            name: 'roteiro'
        }).appendTo('#id-roteiro');
        $.getJSON(path + '/getAulas/' + roteiro, function (data) {
            $.each(data, function (i, obj) {
                var input = $('<input>').attr({
                    type: 'checkbox',
                    name: 'aulas[]',
                    value: obj.id_aula
                });
                $('<tr>').append(
                        $('<td>').append(input),
                        $('<td>').text(obj.nome_aula)
                        ).appendTo('#aulas-table');
            });
        });
    });

    $('.listar-button').on('click', function () {
//        Reseta combos/tabelas e campos ocultos
//        quando é alterado para não haver nenhuma inconsistência.

        resetaTabelaInsumos();
        resetaDivs(['#id-insumos', '#id-fornecedores']);

        //Pesquisa quais checkboxes estão "marcados" e guarda numa variável
        var $checkboxes = $("input[name='aulas[]']:checked");
        var checkboxesIds = "";

        //Para cada checkbox selecionado, concatena numa string os seus respectivos
        //ids, que são, na verdade, seus IDs
        $checkboxes.each(function () {
            checkboxesIds += $(this).val() + "-";
        });

        //Remove o ultimo caracter por culpa do método que adiciona sempre um "-"
        //a mais
        checkboxesIds = checkboxesIds.substr(0, checkboxesIds.length - 1);

        $.getJSON(path + '/getInsumos/' + checkboxesIds, function (data) {
            $.each(data, function (i, obj) {
                $('<input>').attr({
                    type: 'hidden',
                    value: obj.id_insumo,
                    name: 'insumos[]'
                }).appendTo('#id-insumos');

                $('<input>').attr({
                    type: 'hidden',
                    value: obj.insumo_id_fornecedor,
                    name: 'fornecedores[]'
                }).appendTo('#id-fornecedores');

                var qtdTd = $('<input>').attr({
                    type: 'number',
                    value: obj.qtd_total,
                    class: 'input-max-size',
                    name: 'insumos-quantidade[]'
                });
                var obsTd = $('<textarea>').attr({
                    name: 'insumos-observacao[]'
                });
                $('<tr>').append(
                        $('<td>').text(obj.nome_insumo).attr({class: 'nome_insumo'}),
                        $('<td>').text(obj.qtd_atual),
                        $('<td>').append(qtdTd),
                        $('<td>').text(obj.nome_medida),
                        $('<td>').append(obsTd)
                        ).appendTo('#insumos-table');
            });
        });
    });
});
