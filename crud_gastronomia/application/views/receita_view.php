<?php include "templates/header.php"; ?>

<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">

        <?php echo form_open('receita/inserir', 'id="form_receita"'); ?>
        <h1>Insira sua Receita</h1>

        <div class="form-group  label-floating">
            <label for="nome_receita" class="control-label">Nome:</label>
            <input type="text" name="nome_receita" class="form-control campo-validado" value="<?php echo set_value('nome_receita'); ?>" maxlength="100" required/>
        </div>
        <div class="form-group  label-floating">
            <label for="observacao" class="control-label">Observação:</label>
            <input type="text" name="observacao" class="form-control" value="<?php echo set_value('observacao'); ?>" maxlength="200" required/>
        </div>
        <div class="form-group  label-floating">
            <label for="modo_preparo" class="control-label">Modo de Preparo:</label>
            <input type="text" name="modo_preparo" class="form-control" value="<?php echo set_value('modo_preparo'); ?>" maxlength="999" required/>
        </div>
        <label for="receita_id_categoria_receita" >Categoria:</label>
        <select class="form-control" name="receita_id_categoria_receita">
            <option value="0" selected="true">Selecione...</option>
            <?php foreach ($categoria_receita as $cat_receita): ?>
                <option value="<?php echo $cat_receita->id_categoria_receita; ?>"><?php echo $cat_receita->nome_categoria_receita; ?></option>
            <?php endforeach ?>
        </select>
     

        <h1>Insira os Insumos da Receita</h1>
        <div id="insumos">
            <div class="form-group">
                <input type="hidden" class="form-control" name="insumo_receita_id_receita" id="insumo_receita_id_receita" 
                       value="<?php echo set_value('insumo_receita_id_receita', $proximo_id[0]->auto_increment) ?>">
                <input type="hidden" class="form-control" name="id_insumo" id="id_insumo">
                <label for="nome_insumo">Insumo:</label>
                <select class="form-control" name="nome_insumo" id="nome_insumo"></select>
                <label for="quantidade">Quantidade:</label>
                <input type="number" class="form-control" name="quantidade" id="quantidade">
                <button type="button" id="add-insumo" class="form-control">Adicionar</button>
                <br/>
            </div>
            <table class="table text-center" id="insumos-receita">
                <thead>
                    <tr>
                        <th class="text-center">Insumo</th>
                        <th class="text-center">Quantidade</th>
                        <th class="text-center">Medida</th>
<!--                        <th class="text-center">X</th>-->
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <br/>

        <input type="submit" class="btn btn-raised btn-primary"  name="cadastrar" value="Cadastrar" />

        <?php echo form_close(); ?>

        <!-- Lista as Pessoas Cadastradas -->

        <div class="sample">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                <th>Nome</th>
                <th>Editar</th>
                <th>Excluir</th>
                </thead>  
                <tbody>
                    <?php foreach ($receita as $rec): ?>
                        <tr>
                            <td><?php echo $rec->nome_receita; ?></td>
                            <td>
                                <a title="Editar" href="<?php echo base_url() . 'receita/editar/' . $rec->id_receita; ?>">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_edit.png" />
                                </a>
                            </td>
                            <td>
                                <a title="Deletar" href="<?php echo base_url() . 'receita/deletar/' . $rec->id_receita; ?>" onclick="return confirm('Confirma a exclusão deste registro?')">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_delete.png" />
                                </a>
                            </td>   
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</main>
<?php include "templates/footer.php"; ?>