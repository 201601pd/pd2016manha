<?php include "templates/header.php"; ?>
<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">
        
        <?php echo form_open('aula/inserir', 'id="form-aula"'); ?>
        <h1><?php echo $titulo ?></h1>
        <div class="form-group label-floating">
            <label for="nome_aula" class="control-label">Nome da Aula:</label>
            <input type="text" class="form-control campo-validado" name="nome_aula" value="<?php echo set_value('nome_aula'); ?>" maxlength="100" required/>
        </div>
        
        <input type="submit" class="btn btn-raised btn-primary" name="cadastrar" value="Cadastrar" />

        <?php echo form_close(); ?>
        
        <div class="sample">

            <br><br>
            <!-- Lista as aulas cadastradas -->
            <table class="table table-striped table-hover table-responsive">
                <thead>
                <th>Nome da Aula</th>
                <th>Editar</th>
                <th>Excluir</th>
                </thead>  
                <tbody>
                    <?php foreach ($aula as $au): ?>
                        <tr>
                            <td><?php echo $au->nome_aula; ?></td>
                            <td>
                                <a title="Editar" href="<?php echo base_url() . 'aula/editar/' . $au->id_aula; ?>">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_edit.png" />
                                </a>
                            </td>
                            <td>
                                <a title="Deletar" href="<?php echo base_url() . 'aula/deletar/' . $au->id_aula; ?>" onclick="return confirm('Confirma a exclusão deste registro?')">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_delete.png" />
                                </a>
                            </td>    
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</main>
<?php include "templates/footer.php"; ?>