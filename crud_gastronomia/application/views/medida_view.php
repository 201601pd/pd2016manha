<?php include "templates/header.php"; ?>
<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">
        
        <?php echo form_open('medida/inserir', 'id="form-medida"'); ?>
        <h1><?php echo $titulo ?></h1>
        <div class="form-group label-floating">
            <label for="nome_medida" class="control-label">Nome:</label>
            <input type="text" name="nome_medida" class="form-control campo-validado" value="<?php echo set_value('nome_medida'); ?>" maxlength="50" required/>
        </div>
        <input type="submit" class="btn btn-raised btn-primary" name="cadastrar" value="Cadastrar" />

        <?php echo form_close(); ?>
        <br><br>
        <!-- Lista as Pessoas Cadastradas -->
        <div id="grid-pessoas">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                <th>Medida</th>
                <th>Editar</th>
                <th>Excluir</th>
                </thead>
                <tbody>
                    <?php foreach ($medida as $med): ?>
                        <tr>
                            <td> 
                                <?php echo $med->nome_medida ?>
                            </td>
                            <td>
                                <a title="Editar" href="<?php echo base_url() . 'medida/editar/' . $med->id_medida; ?>">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_edit.png" />
                                </a>
                            </td>
                            <td>
                                <a title="Deletar" href="<?php echo base_url() . 'medida/deletar/' . $med->id_medida; ?>" onclick="return confirm('Confirma a exclusão deste registro?')">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_delete.png" />
                                </a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</main>
<?php include "templates/footer.php"; ?>