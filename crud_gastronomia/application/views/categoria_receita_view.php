<?php include "templates/header.php"; ?>
<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">

        <?php echo form_open('categoria_receita/inserir', 'id="form-categoria_receita"'); ?>
        <h1><?php echo $titulo ?></h1>
        <div class="form-group label-floating">
            <label for="nome_categoria_receita" class="control-label">Nome:</label>
            <input type="text" class="form-control campo-validado" name="nome_categoria_receita" value="<?php echo set_value('nome_categoria_receita'); ?>" maxlength="50" required/>
        </div>

        <div class="error"><?php echo form_error('nome_categoria_receita'); ?></div>

        <input type="submit" class="btn btn-raised btn-primary" name="cadastrar" value="Cadastrar" />

        <?php echo form_close(); ?>
        <br><br>
        <div class="sample">


            <!-- Lista as categoria_insumo cadastradas -->
            <table class="table table-striped table-hover table-responsive">
                <thead>
                <th>Nome</th>
                <th>Editar</th>
                <th>Excluir</th>
                </thead>  
                <tbody>
                    <?php foreach ($categoria_receita as $categoria): ?>
                        <tr>
                            <td><?php echo $categoria->nome_categoria_receita; ?></td>
                            <td>
                                <a title="Editar" href="<?php echo base_url() . 'categoria_receita/editar/' . $categoria->id_categoria_receita; ?>">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_edit.png" />
                                </a>
                            </td>
                            <td>
                                <a title="Deletar" href="<?php echo base_url() . 'categoria_receita/deletar/' . $categoria->id_categoria_receita; ?>" onclick="return confirm('Confirma a exclusão deste registro?')">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_delete.png" />
                                </a>
                            </td>    
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</main>
<?php include "templates/footer.php"; ?>