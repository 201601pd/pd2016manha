<?php include "templates/header.php"; ?>
<main>
    <div class="mdl-layout__tab-panel is-active" id="overview">

        <?php echo form_open('solicitacao/gerar', 'id="form-requisicao"'); ?>
        <h1><?php echo $titulo ?></h1>
        
       
            <label class="form-control">Curso</label>
            <?php
            $options = array('' => 'Escolha');
            foreach ($curso as $cur)
                $options[$cur->id_curso] = $cur->nome_curso;
            echo form_dropdown('curso', $options);
            ?>
            
        <br/>
        <label class="form-control">Roteiro</label>
        <?php echo form_dropdown('roteiro', array('' => 'Escolha um Curso'), '', 'id="roteiro"'); ?>
        <br/>
        <label class="form-control">Aulas</label>
        <table class="table text-center" id="aulas-table">
            <tbody>
            </tbody>
        </table>
        <br/>
        <input type="button" name="gerar" value="Listar" class="listar-button mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast"/>
        <br><br>
        <table class="table table-striped table-hover table-responsive" id="insumos-table">
            <thead>
                <tr>
                    <th>Insumo</th>
                    <th>Em Estoque</th>
                    <th>Quantidade Selecionada</th>
                    <th>Medida</th>
                    <th>Observação</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

        <div id="hidden-values">
            <div id="id-curso"></div>
            <div id="id-roteiro" class="center"></div>
            <div id="id-insumos" class="center"></div>
            <div id="id-fornecedores" class="center"></div>
        </div>
        <input type="submit" name="gerar" value="Gerar" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast"/>

        <?php echo form_close(); ?>
    </div>
</main>
<?php include "templates/footer.php"; ?>