<!doctype html>
<!--
  Material Design Lite
  Copyright 2015 Google Inc. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      https://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="................">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/bootstrap-material-design.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/remodal.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/remodal-default-theme.css">
        <!--CSS do calendario-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fullcalendar.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fullcalendar.print.css" media="print">
        <!-- 
        <link href="https://rawgit.com/FezVrasta/bootstrap-material-design/master/dist/css/material-wfont.min.css" rel="stylesheet">
        -->
        <link href="<?php echo base_url(); ?>assets/css/ripples.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/jquery.dropdown.css" rel="stylesheet">

        <title>Requisição de Compras</title>

        <!-- Add to homescreen for Chrome on Android -->
        <meta name="mobile-web-app-capable" content="yes">
        <link rel="icon" sizes="192x192" href="<?php echo base_url(); ?>assets/img/android-desktop.png">

        <!-- Add to homescreen for Safari on iOS -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-title" content="Material Design Lite">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/img/ios-desktop.png">

        <!-- Tile icon for Win8 (144x144 + tile color) -->
        <meta name="msapplication-TileImage" content="<?php echo base_url(); ?>assets/img/touch/ms-touch-icon-144x144-precomposed.png">
        <meta name="msapplication-TileColor" content="#3372DF">

        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.png">

        <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
        <!--
        <link rel="canonical" href="http://www.example.com/">
        -->

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fonts.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/icon.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/material.blue_grey-orange.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css">

        <!--CSS DO DASHBOARD-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/AdminLTE.css">

    </head>
    <body class="mdl-demo mdl-color--grey-100 mdl-color-text--grey-700 mdl-base">

        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
            <header class="mdl-layout__header mdl-layout__header--scroll mdl-color--primary">
                <div class="mdl-layout--large-screen-only mdl-layout__header-row">
                    <h3>Gastronomia &amp; ADS</h3>
                </div>
                <div class="navbar mdl-color--primary-dark">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="navbar-collapse collapse navbar-responsive-collapse">
                            <ul class="nav navbar-nav">
                                <?php if ($this->session->userdata('logged_in')["tipo_usuario"] == 1) { ?>
                                    <li id="home"><a href="<?php echo base_url(); ?>dashboard">Visão Geral</a></li>
                                    <li id="solicitacoes" class="dropdown">
                                        <a href="http://fezvrasta.github.io/bootstrap-material-design/bootstrap-elements.html" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Solicitações
                                            <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo base_url(); ?>solicitacao">Gerar Solicitação</a></li>
                                            <li class="divider"></li>
                                            <li><a href="<?php echo base_url(); ?>solicitacoes_pendentes">Solicitações Pendentes</a></li>
                                        </ul>
                                    </li>
                                <?php } ?>  

                                <?php if (isset($this->session->userdata('logged_in')["session_id"])) { ?>
                                    <li id="manutencao" class="dropdown">
                                        <a href="http://fezvrasta.github.io/bootstrap-material-design/bootstrap-elements.html" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Manutenção
                                            <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <?php if ($this->session->userdata('logged_in')["tipo_usuario"] == 1) { ?>
                                                <li><a href="<?php echo base_url(); ?>curso">Curso</a></li>
                                            <?php } ?>
                                            <li><a href="<?php echo base_url(); ?>aula">Aula</a></li>
                                            <li class="divider"></li>
                                            <li><a href="<?php echo base_url(); ?>receita">Receita</a></li>
                                            <li><a href="<?php echo base_url(); ?>roteiro">Roteiro</a></li>
                                            <li class="divider"></li>
                                            <li><a href="<?php echo base_url(); ?>associacao_aula_roteiro">Associação de Aula e Roteiro</a></li>
                                            <li><a href="<?php echo base_url(); ?>associacao_aula_receita">Associação de Aula e Receita</a></li>
                                            <li class="divider"></li>
                                            <?php if ($this->session->userdata('logged_in')["tipo_usuario"] == 1) { ?>
                                                <li><a href="<?php echo base_url(); ?>categoria_insumo">Categoria de Insumo</a></li>
                                                <li><a href="<?php echo base_url(); ?>categoria_receita">Categoria de Receita</a></li>
                                                <li><a href="<?php echo base_url(); ?>medida">Medida</a></li>
                                                <li><a href="<?php echo base_url(); ?>fornecedor">Fornecedor</a></li>
                                                <li><a href="<?php echo base_url(); ?>insumo">Insumo</a></li>
                                            <?php } ?>

                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($this->session->userdata('logged_in')["tipo_usuario"] == 1) { ?>
                                    <li id="configuracoes" class="dropdown">
                                        <a href="http://fezvrasta.github.io/bootstrap-material-design/bootstrap-elements.html" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Configurações
                                            <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo base_url(); ?>usuario">Usuário</a></li>
                                            <li><a href="<?php echo base_url(); ?>tipo_acesso">Tipo de Acesso</a></li>
                                        </ul>
                                    </li>
                                <?php } ?> 
                                <?php if (isset($this->session->userdata('logged_in')["session_id"])) { ?>
                                    <li><a href="<?php echo base_url(); ?>logout">Sair</a></li>
                                <?php } ?> 
                            </ul>
                        </div>
                    </div>
                </div>


            </header>



