<footer class="mdl-mega-footer">
    <div class="mdl-mega-footer--middle-section">
        <div class="mdl-mega-footer--drop-down-section">
            <input class="mdl-mega-footer--heading-checkbox" type="checkbox" checked>
            <h1 class="mdl-mega-footer--heading">Localização</h1>
            <ul class="mdl-mega-footer--link-list">
                <li>Rua Coronel Genuíno, 130</li>
                <li>Centro Histórico - Porto Alegre/RS</li>
                <li>CEP: 90010-350</li>
                <li>Tel: (51) 3022-1044</li>
            </ul>
        </div>
        <div class="mdl-mega-footer--drop-down-section col-md-push-8">
            <input class="mdl-mega-footer--heading-checkbox" type="checkbox" checked>
            <h1 class="mdl-mega-footer--heading">Redes Sociais</h1>
            <ul class="mdl-mega-footer--link-list">
                <li style="margin-left: -8px;"><a href="http://www.facebook.com/senacrsoficial"><img src="../frontEnd/images/icone_facebook.png"/></a>
                    <a href="http://www.twitter.com/senacrs"><img src="../frontEnd/images/icone_twitter.png"/></a></li>

            </ul>
        </div>

</footer>

</div>
<!--Modal início-->
<div class="remodal" data-remodal-id="modal">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h1>Campo já cadastrado!</h1>
    <p>
        O campo com este valor já está cadastrado!
    </p>
    <br>
    <button data-remodal-action="confirm" class="remodal-confirm">OK</button>
</div>
<!--Modal fim-->


<!--Obs.: Todos estes scripts estão sendo carregados cada vez que uma página é carregada. 
Isso se deve ao fato de que a atual estrutura dos templates não permitiria carregar os scripts
respectivos separadamente de cada página -->

<script src="<?php echo base_url(); ?>assets/js/fullcalendar.js"></script>
<script src="<?php echo base_url(); ?>assets/js/moment.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/material.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/ripples.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/remodal.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.dropdown.js"></script>


<script>
    $(function () {
        $.material.init();

        $(".select").dropdown({"autoinit": ".select"});
        $(".select").dropdown({"optionClass": "withripple"});
        $("#dropdown-menu select").dropdown();
    });
</script>
<script>
    $(function () {
        var site_url = "<?php echo site_url(); ?>";
        var count = 0;

        //Envia uma requisição AJAX para o controlador de receita
        //e retorna um json com todos insumos cadastrados
        $.getJSON(site_url + "receita/json_insumos", function (json_data) {
            $('#nome_insumo').empty();
            $('#nome_insumo').append($('<option>').text("Selecione").attr('value', 0));
            $.each(json_data, function (i, obj) {
                $('#nome_insumo').append($('<option>').text(obj.nome).attr('value', obj.id).attr('name', obj.medida));
            });
        });

        /*-------------------*/
        //Quando o botão de id = "add-insumo" for acionado, a função executa verificações
        //para identificar se os requisitos para adicionar um insumo foram preenchidos.
        //Se as verificações ocorrerem como esperado, um insumo e seus detalhes são
        //adicionados a uma tabela de id = "insumos".
        $('#add-insumo').click(function () {
            count += 1;
            var nome_insumo = $(this).closest('.form-group').find('#nome_insumo option:selected');
            var quantidade_insumo = $(this).closest('.form-group').find('#quantidade');

            console.log(nome_insumo.text());
            console.log(quantidade_insumo.val());
            console.log($("[name='insumos[]']").val());
            if (nome_insumo.text() != 'Selecione' && quantidade_insumo.val() != '') {
                var alreadyExists = false;
                if ($("[name='insumos[]']").val() == nome_insumo.val()) {
                    alreadyExists = true;

                }
                if (!alreadyExists) {
                    $('<tr>').append(
                            $('<td>').text(nome_insumo.text()),
                            $('<td>').text(quantidade_insumo.val()),
                            $('<td>').text(nome_insumo.attr('name'))
                            ).appendTo('#insumos-receita');
                    $('<input>').attr({
                        type: 'text',
                        id: 'ins' + count,
                        value: nome_insumo.val(),
                        name: 'insumos[]'
                    }).appendTo('#insumos');
                    $('<input>').attr({
                        type: 'hidden',
                        id: 'qtd' + count,
                        value: quantidade_insumo.val(),
                        name: 'quantidades[]'
                    }).appendTo('#insumos');
                }
            } else {

            }
            quantidade_insumo.val('');
        });
    });
</script>
<script>
    
    //Gera os arrays relacionando a sua aba e suas possíveis páginas, respectivamente
    function generatePageArrays() {
        //var home = []; -> não possui ainda nenhum controlador associado
        var solicitacoes = ["firstArray", "solicitacao"];
        var manutencao = ["secondArray", "curso", "aula", "receita", "roteiro", "associacao_aula_roteiro",
            "categoria_insumo", "categoria_receita", "medida", "fornecedor", "insumo"];
        var configuracoes = ["thirdArray", "usuario", "tipo_acesso"];

        var mainPages = [solicitacoes, manutencao, configuracoes];

        return mainPages;
    }

    //Ao carregar a página, o método compara a url local com os arrays préviamente
    //gerados, e com isso consegue assimilar onde ele está, atribuindo qual aba
    //deve receber a classe active (sem jQuery \o/).
    window.onload = function () {
        var mainPages = generatePageArrays();
        var url = window.location.href;

        for (var i = 0; i < mainPages.length; i++) {
            for (var j = 1; j < mainPages[i].length; j++) {
                if (url.includes(mainPages[i][j])) {
                    switch (mainPages[i][0]) {
                        case "firstArray":
                            document.getElementById("solicitacoes").className += " active";
                            break;
                        case "secondArray":
                            document.getElementById("manutencao").className += " active";
                            break;
                        case "thirdArray":
                            document.getElementById("configuracoes").className += " active";
                            break;
                    }
                    return;
                }
            }
        }
    };
</script>
<script src="<?php echo base_url(); ?>assets/js/custom/verifica-campo.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom/requisicao-reducer.js"></script>
<!-- <script src="https://code.getmdl.io/1.1.3/material.min.js"></script> -->

<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script> 

</body>
</html>