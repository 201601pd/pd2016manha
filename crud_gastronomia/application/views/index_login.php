<?php include "templates/header.php"; ?>
<main class="mdl-layout__content">
    
    <div class="mdl-layout__tab-panel is-active" id="overview">

        <?php echo validation_errors(); ?>
        <?php echo form_open('verifica_login'); ?>

        <h1><?php echo $titulo ?></h1>
        <br>
        <div class="form-group label-floating">
            <label for="matricula"  class="control-label">Matricula:</label>
            <input type="text" size="20" id="matricula" name="matricula"  class="form-control"/>
        </div>

        <div class="form-group label-floating">
            <label for="senha" class="control-label">Senha:</label>
            <input type="password" size="20" id="senha" name="senha"  class="form-control"/>
        </div>

        <input type="submit" value="Login" class="btn btn-raised btn-primary"/>
        <?php echo form_close(); ?>
    </div>
</main>
<?php include "templates/footer.php"; ?>