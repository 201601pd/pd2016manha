<?php include "templates/header.php"; ?>
<main class="mdl-layout__content">

    <div class="mdl-layout__tab-panel is-active" id="overview">

        <?php echo form_open('fornecedor/atualizar', 'id="form-fornecedor"'); ?>
        <h1><?php echo $titulo ?></h1>

        <input type="hidden" id="id-validado" name="id_fornecedor" value="<?php echo $dados_fornecedor[0]->id_fornecedor; ?>"/>
        
        <div class="form-group label-floating">
            <label for="cnpj" class="control-label">CNPJ:</label>
            <input class="cnpj form-control campo-validado" type="text" name="cnpj" value="<?php echo $dados_fornecedor[0]->cnpj; ?>" required/>
        </div>

        <div class="form-group label-floating">
            <label for="status_fornecedor" class="control-label">Status:</label>
            <select class="form-control" name="status_fornecedor"> 
                <option value="1" <?php if ($dados_fornecedor[0]->status_fornecedor == 1) echo 'selected="true"'; ?>>Ativo</option>
                <option value="0" <?php if ($dados_fornecedor[0]->status_fornecedor == 0) echo 'selected="true"'; ?>>Inativo</option>
            </select>
        </div>

        <div class="form-group label-floating">
            <label for="razao_social" class="control-label">Razão Social:</label><br/>
            <input type="text" class="form-control" name="razao_social" value="<?php echo $dados_fornecedor[0]->razao_social; ?>" maxlength="100" required/>
        </div>

        <div class="form-group label-floating">
            <label for="endereco" class="control-label">Endereço:</label><br/>
            <input type="text" class="form-control" name="endereco" value="<?php echo $dados_fornecedor[0]->endereco; ?>" maxlength="150" required/>
        </div>

        <div class="form-group label-floating">
            <label for="telefone" class="control-label">Telefone:</label><br/>
            <input class="telefone form-control" type="text" name="telefone" value="<?php echo $dados_fornecedor[0]->telefone; ?>" required/>
        </div>

        <div class="form-group label-floating">
            <label for="email" class="control-label">Email:</label><br/>
            <input type="text" class="form-control" name="email" value="<?php echo $dados_fornecedor[0]->email; ?>" maxlength="70" required/>
        </div>
        </br>

        <input type="submit" class="btn btn-raised btn-primary" name="atualizar" value="Atualizar" />

        <?php echo form_close(); ?>

        <script src="<?php echo base_url(); ?>assets/js/jquery-1.12.2.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.mask.js"></script>
        <script>
            $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
            $('.telefone').mask('(00) 0000-00000');
        </script>
    </div>
</main>
<?php include "templates/footer.php"; ?>