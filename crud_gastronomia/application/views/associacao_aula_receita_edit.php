<?php include "templates/header.php"; ?>
<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">
        <?php echo form_open('associacao_aula_receita/atualizar', 'id="form-associacao_aula_receita"'); ?>

        <input type="hidden" name="old_aula_receita_id_aula" value="<?php echo $dados_associacao_aula_receita[0]->aula_receita_id_aula; ?>"/>
        <input type="hidden" name="old_aula_receita_id_receita" value="<?php echo $dados_associacao_aula_receita[0]->aula_receita_id_receita; ?>"/>

       <h1><?php echo $titulo ?></h1>

        <div class="form-group">
            <label for="aula_receita_id_aula">Aula:</label><br/>
            <select class="form-control" name="aula_receita_id_aula">
                <option value="0" selected="true">Selecione...</option>
                <?php foreach ($aula as $au): ?>
                    <option value="<?php echo $dados_associacao_aula_receita[0]->aula_receita_id_aula; ?>" 
                            <?php if ($au->id_aula === $dados_associacao_aula_receita[0]->aula_receita_id_aula) echo "selected" ?>><?php echo $au->nome_aula; ?></option>
                        <?php endforeach ?>
            </select>
        </div>

        <div class="form-group">
            <label for="aula_receita_id_receita">Receita pertencente a aula:</label><br/>
            <select class="form-control" name="aula_receita_id_receita">
                <?php foreach ($receita as $rec): ?>
                    <option value="<?php echo $rec->id_receita; ?>" <?php if ($rec->id_receita === $dados_associacao_aula_receita[0]->aula_receita_id_receita) echo "selected" ?>>
                        <?php echo $rec->nome_receita; ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>

        <input type="submit" class="btn btn-raised btn-primary" name="atualizar" value="Atualizar" />

        <?php echo form_close(); ?>
    </div>
</main>
<?php include "templates/footer.php"; ?>