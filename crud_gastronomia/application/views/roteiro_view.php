<?php include "templates/header.php"; ?>

<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">
        <?php echo form_open('roteiro/inserir', 'id="form-roteiro"'); ?>

        <h1><?php echo $titulo ?></h1>
        <div class="form-group  label-floating">
            <label for="titulo" class="control-label">Titulo:</label>
            <input type="text" name="titulo" class="form-control" value="<?php echo set_value('titulo'); ?>" maxlength="50" required/>
            <br/>
        </div>
        <div class="form-group label-floating">
            <label for="roteiro_id_curso">Curso:</label>
            <select class="form-control" name="roteiro_id_curso" required>
                <option value="0" selected="true">Selecione...</option>
                <?php foreach ($curso as $cur): ?>
                    <option value="<?php echo $cur->id_curso; ?>"><?php echo $cur->nome_curso; ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <input type="submit" class="btn btn-raised btn-primary"  name="cadastrar" value="Cadastrar" />

        <?php echo form_close(); ?>

        <div class="sample">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th>Titulo</th>
                        <th>Editar</th>
                        <th>Excluir</th>
                    </tr>
                </thead>  
                <tbody>
                    <?php foreach ($roteiro as $rot): ?>
                        <tr>
                            <td><?php echo $rot->titulo; ?></td>
                            <td>
                                <a title="Editar" href="<?php echo base_url() . 'roteiro/editar/' . $rot->id_roteiro; ?>">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_edit.png" />
                                </a>
                            </td>
                            <td>
                                <a title="Deletar" href="<?php echo base_url() . 'roteiro/deletar/' . $rot->id_roteiro; ?>" onclick="return confirm('Confirma a exclusão deste registro?')">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_delete.png" />
                                </a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</main>
<?php include "templates/footer.php"; ?>