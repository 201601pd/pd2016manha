<?php include "templates/header.php"; ?>

<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">
        <?php echo form_open('insumo/atualizar', 'id="form-insumo"'); ?>
        <h1><?php echo $titulo ?></h1>

        <br>
        <input type="hidden" name="id_insumo" value="<?php echo $dados_insumo[0]->id_insumo; ?>"/>

        <div class="form-group has-success label-floating">
            <label for="nome_insumo" class="control-label">Nome:</label><br/>
            <input type="text" class="form-control" name="nome_insumo" value="<?php echo $dados_insumo[0]->nome_insumo; ?>" maxlength="100" required//>
        </div>

        <div class="form-group has-success label-floating">
            <label for="qtd_minima" class="control-label">Quantidade Mínima:</label><br/>
            <input type="number" name="qtd_minima" class="form-control" value="<?php echo $dados_insumo[0]->qtd_minima; ?>" min="0" required/>
        </div>

        <div class="form-group has-success label-floating">
            <label for="qtd_atual" class="control-label">Quantidade Atual:</label><br/>
            <input type="number" name="qtd_atual" class="form-control" value="<?php echo $dados_insumo[0]->qtd_atual; ?>" min="0" required/>
        </div>

        <div class="form-group">
            <label for="perecivel">Perecível:</label><br/>
            <select class="form-control" name="perecivel"> 
                <option value="1" <?php if ($dados_insumo[0]->perecivel == 1) echo 'selected'; ?>>Sim</option>
                <option value="0" <?php if ($dados_insumo[0]->perecivel == 0) echo 'selected'; ?>>Não</option>
            </select>
        </div>

        <div class="form-group">
            <label for="insumo_id_medida">Medida:</label><br/>
            <select name="insumo_id_medida" class="form-control">
                <?php foreach ($medida as $med): ?>
                    <option name="insumo_id_medida" 
                            value="<?php echo $med->id_medida; ?>" <?php if ($med->id_medida === $dados_insumo[0]->insumo_id_medida) echo "selected" ?>>
                                <?php echo $med->nome_medida; ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>

        <div class="form-group">
            <label for="insumo_id_categoria_insumo">Categoria:</label><br/>
            <select name="insumo_id_categoria_insumo" class="form-control">
                <?php foreach ($categoria_insumo as $cat): ?>
                    <option name="insumo_id_categoria_insumo" 
                            value="<?php echo $cat->id_categoria_insumo; ?>" <?php if ($cat->id_categoria_insumo === $dados_insumo[0]->insumo_id_categoria_insumo) echo "selected" ?>>
                                <?php echo $cat->nome_categoria_insumo; ?>
                    </option>
                <?php endforeach ?>           
            </select>
        </div>

        <div class="form-group">
            <label for="insumo_id_fornecedor">Fornecedor:</label><br/>
            <select name="insumo_id_fornecedor" class="form-control">
                <?php foreach ($fornecedor as $forn): ?>
                    <option name="insumo_id_fornecedor" 
                            value="<?php echo $forn->id_fornecedor; ?>" <?php if ($forn->id_fornecedor === $dados_insumo[0]->insumo_id_fornecedor) echo "selected" ?>>
                                <?php echo $forn->razao_social; ?>
                    </option>
                <?php endforeach ?>           
            </select>
        </div>

        <input type="submit" name="atualizar" value="Atualizar" class="btn btn-raised btn-primary"/>

        <?php echo form_close(); ?>
    </div>

</main>

<?php include "templates/footer.php"; ?>