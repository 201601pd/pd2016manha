<?php include "templates/header.php"; ?>

<main class="mdl-layout__content">
    
    <div class="mdl-layout__tab-panel is-active"  id="overview">
        
        <?php echo form_open('curso/atualizar', 'id="form-curso"'); ?>
        <h1><?php echo $titulo ?></h1>
        <input type="hidden" id="id-validado" name="id_curso" value="<?php echo $dados_curso[0]->id_curso; ?>"/>

        <div class="form-group label-floating">
            <label for="nome_curso" class="control-label">Nome:</label>
            <input type="text" class="form-control campo-validado" name="nome_curso" value="<?php echo $dados_curso[0]->nome_curso; ?>" maxlength="50" required/>
        </div>

        <div class="form-group label-floating">
            <label for="descricao" class="control-label">Descrição:</label>
            <input type="text" class="form-control" name="descricao" value="<?php echo $dados_curso[0]->descricao; ?>" maxlength="200" required/>
        </div>
        </br>
        <input type="submit" class="btn btn-raised btn-primary" name="atualizar" value="Atualizar" />

        <?php echo form_close(); ?>
    </div>

</main>
<?php include "templates/footer.php"; ?>