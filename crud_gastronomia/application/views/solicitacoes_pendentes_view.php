<?php include "templates/header.php"; ?>
<main>
    <div class="mdl-layout__tab-panel is-active" id="overview">

        <h1>Solicitações pendentes</h1>
        <?php foreach ($solicitacao as $soli): ?>
            <h6><?php foreach ($fornecedor as $forn):
                    if($soli->solicitacao_id_fornecedor == $forn->id_fornecedor){
                        echo $forn->razao_social.' - '.$soli->data_requisicao;
                    }
                endforeach; ?></h6>

            <table class="table table-striped table-hover table-responsive" id="insumos-table">
                <thead>
                    <tr>
                        <th>Insumo</th>
                        <th>Quantidade</th>
                        <th>Observação</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($solicitacao_insumo as $soli_ins): ?>
                    <?php if($soli_ins->solicitacao_insumo_id_solicitacao == $soli->id_solicitacao){ ?>
                    <tr>
                        <td>
                            <?php foreach ($insumo as $ins):
                                if($soli_ins->solicitacao_insumo_id_insumo == $ins->id_insumo){
                                    echo $ins->nome_insumo;
                                }
                            endforeach; ?>
                        </td>
                        <td><?php echo $soli_ins->quantidade; ?></td>
                        <td><?php echo $soli_ins->observacao; ?></td>
                    </tr>
                    <?php }
                    endforeach ?>
                </tbody>
            </table>
            <br/>
        <?php endforeach ?>
    </div>
</main>
<?php include "templates/footer.php"; ?>