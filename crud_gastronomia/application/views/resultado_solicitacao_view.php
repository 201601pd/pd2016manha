<?php include "templates/header.php"; ?>
<main>
    <div class="mdl-layout__tab-panel is-active" id="overview">

        <?php for ($i = 0; $i < count($solicitacoes_geradas); $i++) { ?>
        <h1><?php echo $solicitacoes_geradas[$i][0]->razao_social ?></h1>
            <table class="table table-striped table-hover table-responsive" id="insumos-table">
                <thead>
                    <tr>
                        <th>Insumo</th>
                        <th>Quantidade</th>
                        <th>Observação</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for ($j = 0; $j < count($solicitacoes_geradas[$i]); $j++) { ?>
                        <tr>
                            <td><?php echo $solicitacoes_geradas[$i][$j]->nome_insumo; ?></td>
                            <td><?php echo $solicitacoes_geradas[$i][$j]->quantidade; ?></td>
                            <td><?php echo $solicitacoes_geradas[$i][$j]->observacao; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <br/>
        <?php } ?>
    </div>
</main>
<?php include "templates/footer.php"; ?>