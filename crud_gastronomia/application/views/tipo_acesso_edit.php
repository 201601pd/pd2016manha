<?php include "templates/header.php"; ?>

<main class="mdl-layout__content">
    
    <div class="mdl-layout__tab-panel is-active" id="overview">
        
        <?php echo form_open('tipo_acesso/atualizar', 'id="form-tipo_acesso"'); ?>
        <h1><?php echo $titulo ?></h1>
        <input type="hidden" id="id-validado" name="id_tipo_acesso" value="<?php echo $dados_tipo_acesso[0]->id_tipo_acesso; ?>"/>

        <div class="form-group label-floating">
            <label for="nome_tipo_acesso" class="control-label">Tipo de Acesso:</label><br/>
            <input class="form-control campo-validado" type="text" name="nome_tipo_acesso" value="<?php echo $dados_tipo_acesso[0]->nome_tipo_acesso; ?>" maxlength="50" required/>
        </div>

        <div class="form-group label-floating">
            <label for="descricao" class="control-label">Descrição:</label><br/>
            <input class="form-control" type="text" name="descricao" value="<?php echo $dados_tipo_acesso[0]->descricao; ?>" maxlength="200" required/>
        </div>
        </br>

        <input type="submit" class="btn btn-raised btn-primary" name="atualizar" value="Atualizar" />

        <?php echo form_close(); ?>
    </div>
</main>
<?php include "templates/footer.php"; ?>
        