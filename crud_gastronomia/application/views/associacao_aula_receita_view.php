<?php include "templates/header.php"; ?>
<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">
        <?php echo form_open('associacao_aula_receita/inserir', 'id="form-associacao_aula_receita"'); ?>

        <h1><?php echo $titulo ?></h1>

        <div class="form-group label-floating">
            <label for="aula_receita_id_aula">Aula:</label><br/>
            <select class="form-control" name="aula_receita_id_aula">
                <option value="0" selected="true">Selecione...</option>
                <?php foreach ($aula as $au): ?>
                    <option value="<?php echo $au->id_aula; ?>"><?php echo $au->nome_aula; ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <div class="form-group label-floating">
            <label for="aula_receita_id_receita">Receita pertencente a aula:</label><br/>
            <select class="form-control" name="aula_receita_id_receita">
                <option value="0" selected="true">Selecione...</option>
                <?php foreach ($receita as $rec): ?>
                    <option value="<?php echo $rec->id_receita; ?>"><?php echo $rec->nome_receita; ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <input type="submit" class="btn btn-raised btn-primary" name="cadastrar" value="Cadastrar" />

        <?php echo form_close(); ?>

        <div class="sample">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                <th>Aula</th>
                <th>Receita</th>
                <th>Editar</th>
                <th>Excluir</th>
                </thead>  
                <tbody>
                    <?php foreach ($aula_receita as $au_rec): ?>
                        <tr>
                            <td>
                                <?php foreach ($aula as $au): ?>
                                    <?php if ($au->id_aula === $au_rec->aula_receita_id_aula) echo $au->nome_aula; ?>
                                <?php endforeach ?>
                            </td>
                            <td>
                                <?php foreach ($receita as $rec): ?>
                                    <?php if ($rec->id_receita === $au_rec->aula_receita_id_receita) echo $rec->nome_receita; ?>
                                <?php endforeach ?>
                            </td>    
                            <td>
                                <a title="Editar" href="<?php echo base_url() . 'associacao_aula_receita/editar/' . $au_rec->aula_receita_id_aula . '-' . $au_rec->aula_receita_id_receita; ?>">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_edit.png" />
                                </a>
                            </td>
                            <td>
                                <a title="Deletar" href="<?php echo base_url() . 'associacao_aula_receita/deletar/' . $au_rec->aula_receita_id_aula . '-' . $au_rec->aula_receita_id_receita; ?>" onclick="return confirm('Confirma a exclusão deste registro?')">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_delete.png" />
                                </a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</main>
<?php include "templates/footer.php"; ?>