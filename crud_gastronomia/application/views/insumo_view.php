<?php include "templates/header.php"; ?>

<main class="mdl-layout__content">
    <br>
    <div class="mdl-layout__tab-panel is-active" id="overview">

        <?php echo form_open('insumo/inserir', 'id="form-insumo"'); ?>
        <h1><?php echo $titulo ?></h1>
        <div class="form-group label-floating">
            <label for="nome_insumo" class="control-label">Nome:</label>
            <input type="text" class="form-control" name="nome_insumo" value="<?php echo set_value('nome_insumo'); ?>" maxlength="100" required/>
        </div>

        <div class="form-group label-floating">
            <label for="qtd_minima" class="control-label">Quantidade Mínima:</label>
            <input class="form-control" type="number" name="qtd_minima" value="<?php echo set_value('qtd_minima'); ?>" min="0" required/>
        </div>

        <div class="form-group label-floating">
            <label for="qtd_atual" class="control-label">Quantidade Atual:</label>
            <input type="number" class="form-control" name="qtd_atual" value="<?php echo set_value('qtd_atual'); ?>" min="0" required/>
        </div>

        <div class="form-group"> 
            <label for="perecivel">Perecível:</label><br/>
            <select class="form-control" name="perecivel">
                <option value="1" selected="true">Sim</option>
                <option value="0">Não</option>
            </select>
        </div>

        <div class="form-group">
            <label for="insumo_id_medida">Medida:</label><br/>
            <select class="form-control" name="insumo_id_medida">
                <option value="0" selected="true">Selecione...</option>  
                <?php foreach ($medida as $med): ?>
                    <option value="<?php echo $med->id_medida; ?>"><?php echo $med->nome_medida; ?></option>
                <?php endforeach ?>
            </select>
        </div>    

        <div class="form-group">
            <label for="insumo_id_categoria_insumo">Categoria:</label><br/>
            <select class="form-control" name="insumo_id_categoria_insumo">
                <option value="0" selected="true">Selecione...</option>
                <?php foreach ($categoria_insumo as $cat): ?>
                    <option value="<?php echo $cat->id_categoria_insumo; ?>"><?php echo $cat->nome_categoria_insumo; ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <div class="form-group">
            <label for="insumo_id_fornecedor">Fornecedor:</label><br/>
            <select class="form-control" name="insumo_id_fornecedor">
                <option value="0" selected="true">Selecione...</option>
                <?php foreach ($fornecedor as $forn): ?>
                    <option value="<?php echo $forn->id_fornecedor; ?>"><?php echo $forn->razao_social; ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <input type="submit" class="btn btn-raised btn-primary" name="cadastrar" value="Cadastrar" />

        <?php echo form_close(); ?>

    </div>

</main>

<section class="listagem">

    <div class="sample">


        <!-- Lista as insumos cadastradas -->
        <table class="table table-striped table-hover table-responsive">
            <thead>
            <th>Insumo</th>
            <th>Quantidade Mínima</th>
            <th>Quantidade Atual</th>
            <th>Perecível</th>
            <th>Medida</th>
            <th>Categoria</th>
            <th>Fornecedor</th>
            <th>Editar</th>
            <th>Excluir</th>
            </thead>  
            <tbody>
                <?php foreach ($insumo as $ins): ?>
                    <tr>
                        <td><?php echo $ins->nome_insumo; ?></td>
                        <td><?php echo $ins->qtd_minima; ?></td>
                        <td><?php echo $ins->qtd_atual; ?></td>
                        <td><?php echo ($ins->perecivel == 0 ? "Não" : "Sim"); ?></td>
                        <td>
                            <?php foreach ($medida as $med): ?>
                                <?php if ($med->id_medida === $ins->insumo_id_medida) echo $med->nome_medida; ?>
                            <?php endforeach ?>
                        </td>
                        <td>
                            <?php foreach ($categoria_insumo as $cat): ?>
                                <?php if ($cat->id_categoria_insumo === $ins->insumo_id_categoria_insumo) echo $cat->nome_categoria_insumo; ?>
                            <?php endforeach ?>
                        </td>
                        <td>
                            <?php foreach ($fornecedor as $forn): ?>
                                <?php if ($forn->id_fornecedor === $ins->insumo_id_fornecedor) echo $forn->razao_social; ?>
                            <?php endforeach ?>
                        </td>
                        <td>
                            <a title="Editar" href="<?php echo base_url() . 'insumo/editar/' . $ins->id_insumo; ?>">
                                <img src="<?php echo base_url(); ?>assets/img/icon_edit.png" />
                            </a>
                        </td>
                        <td>
                            <a title="Deletar" href="<?php echo base_url() . 'insumo/deletar/' . $ins->id_insumo; ?>" onclick="return confirm('Confirma a exclusão deste registro?')">
                                <img src="<?php echo base_url(); ?>assets/img/icon_delete.png" />
                            </a>
                        </td>    
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>

    </div>

</section>

<?php include "templates/footer.php"; ?>

