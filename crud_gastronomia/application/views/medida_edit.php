<?php include "templates/header.php"; ?>
<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">

        <?php echo form_open('medida/atualizar', 'id="form-medida"'); ?>
        <h1><?php echo $titulo ?></h1>
        <input type="hidden" id="id-validado" name="id_medida" value="<?php echo $dados_medida[0]->id_medida; ?>"/>

        <div class="form-group">
            <label for="nome_medida" class="control-label">Nome:</label><br/>
            <input type="text" name="nome_medida" class="form-control campo-validado" value="<?php echo $dados_medida[0]->nome_medida; ?>" maxlength="50" required/>
        </div>
        <input type="submit" class="btn btn-raised btn-primary" name="atualizar" value="Atualizar" />

        <?php echo form_close(); ?>
    </div>

</main>

<?php include "templates/footer.php"; ?>
