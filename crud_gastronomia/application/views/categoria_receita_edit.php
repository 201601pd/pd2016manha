<?php include "templates/header.php"; ?>
<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">

        <?php echo form_open('categoria_receita/atualizar', 'id="form-categoria_receita"'); ?>
        <h1><?php echo $titulo ?></h1>
        <input type="hidden" id="id-validado" name="id_categoria_receita" value="<?php echo $dados_categoria_receita[0]->id_categoria_receita; ?>"/>
        <br>
        <div class="form-group label-floating">
            <label for="nome_categoria_receita" class="control-label">Nome:</label>
            <input type="text" class="form-control campo-validado" name="nome_categoria_receita" value="<?php echo $dados_categoria_receita[0]->nome_categoria_receita; ?>" maxlength="50" required/>
        </div>

        <div class="error"><?php echo form_error('nome_categoria_receita'); ?></div>

        <input type="submit" class="btn btn-raised btn-primary" name="atualizar" value="Atualizar" />

        <?php echo form_close(); ?>

    </div>
</main>
<?php include "templates/footer.php"; ?>