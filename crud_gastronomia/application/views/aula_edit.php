<?php include "templates/header.php"; ?>
<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">

        <?php echo form_open('aula/atualizar', 'id="form-aula"'); ?>
        <h1><?php echo $titulo ?></h1>
        <br>
        <input type="hidden" id="id-validado" name="id_aula" value="<?php echo $dados_aula[0]->id_aula; ?>"/>

        <div class="form-group label-floating">
            <label for="nome_aula" class="control-label">Nome da Aula:</label>
            <input type="text" class="form-control campo-validado" name="nome_aula" value="<?php echo $dados_aula[0]->nome_aula; ?>" maxlength="100" required/>
            <br/>
        </div>

        <input type="submit" class="btn btn-raised btn-primary" name="atualizar" value="Atualizar" />

        <?php echo form_close(); ?>

    </div>
</main>
<?php include "templates/footer.php"; ?>