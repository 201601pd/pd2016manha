<?php include "templates/header.php"; ?>

<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">
        <?php echo form_open('roteiro/atualizar', 'id="form-roteiro"'); ?>

        <h1><?php echo $titulo ?></h1>
        
        <input type="hidden" class="form-control" name="id_roteiro" value="<?php echo $dados_roteiro[0]->id_roteiro; ?>" />
        <div class="form-group label-floating">
            <label for="titulo" class="control-label">Titulo:</label>
            <input type="text" name="titulo" class="form-control" value="<?php echo $dados_roteiro[0]->titulo; ?>" maxlength="50" required/>    
        </div>
        
        <div class="form-group">
            <label for="roteiro_id_curso">Curso:</label>
            <select name="roteiro_id_curso"  class="form-control" required>
                <?php foreach ($curso as $cur): ?>
                    <option name="roteiro_id_curso" 
                            value="<?php echo $cur->id_curso; ?>" <?php if ($cur->id_curso === $dados_roteiro[0]->roteiro_id_curso) echo "selected" ?>>
                                <?php echo $cur->nome_curso; ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>
        
        <input type="submit" class="btn btn-raised btn-primary" name="atualizar" value="Atualizar" />

        <?php echo form_close(); ?>
    </div>
</main>
<?php include "templates/footer.php"; ?>