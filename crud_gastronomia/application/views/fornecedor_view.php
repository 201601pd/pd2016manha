<?php include "templates/header.php"; ?>
<main class="mdl-layout__content">

    <div class="mdl-layout__tab-panel is-active" id="overview">

        <?php echo form_open('fornecedor/inserir', 'id="form-fornecedor"'); ?>
        <h1><?php echo $titulo ?></h1>
        
        <div class="form-group label-floating">
            <label for="cnpj" class="control-label">CNPJ:</label>
            <input class="cnpj form-control campo-validado" type="text" name="cnpj" value="<?php echo set_value('cnpj'); ?>" required/>
        </div>

        <div class="form-group label-floating">
            <label for="status_fornecedor" class="control-label">Status:</label><br/>
            <select class="form-control" name="status_fornecedor">
                <option value="1" selected="true">Ativo</option>
                <option value="0">Inativo</option>
            </select>
        </div>

        <div class="form-group label-floating">
            <label for="razao_social" class="control-label">Razão Social:</label>
            <input type="text" class="form-control" name="razao_social" value="<?php echo set_value('razao_social'); ?>" maxlength="100" required/>
        </div>

        <div class="form-group label-floating">
            <label for="endereco" class="control-label">Endereço:</label>
            <input type="text" class="form-control" name="endereco" value="<?php echo set_value('endereco'); ?>" maxlength="150" required/>
        </div>

        <div class="form-group label-floating">
            <label for="telefone" class="control-label">Telefone:</label>
            <input class="telefone form-control" type="text" name="telefone" value="<?php echo set_value('telefone'); ?>" required/>
        </div>

        <div class="form-group label-floating">
            <label for="email" class="control-label">Email:</label>
            <input type="email" class="form-control" name="email" value="<?php echo set_value('email'); ?>" maxlength="150" required/>
        </div>
        </br>

        <input type="submit" class="btn btn-raised btn-primary" name="cadastrar" value="Cadastrar" />

        <?php echo form_close(); ?>




        <script src="<?php echo base_url(); ?>assets/js/jquery-1.12.2.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.mask.js"></script>
        <script>
            $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
            $('.telefone').mask('(00) 0000-00000');
        </script>
    </div>
</main>
<!-- Lista as Fornecedores cadastrados -->
<section class="listagem">
    <div class="sample">
        <table class="table table-striped table-hover table-responsive">
            <thead>
            <th>CNPJ</th>
            <th>Status</th>
            <th>Razão Social</th>
            <th>Endereço</th>
            <th>Telefone</th>
            <th>e-mail</th>
            <th>Editar</th>
            <th>Excluir</th>
            </thead>  
            <tbody>
                <?php foreach ($fornecedor as $fornec): ?>
                    <tr>
                        <td class="cnpj"><?php echo $fornec->cnpj; ?></td>
                        <td><?php echo ($fornec->status_fornecedor == 0 ? "Não" : "Sim"); ?></td>
                        <td><?php echo $fornec->razao_social; ?></td>
                        <td><?php echo $fornec->endereco; ?></td>
                        <td class="telefone"><?php echo $fornec->telefone; ?></td>
                        <td><?php echo $fornec->email; ?></td>
                        <td>
                            <a title="Editar" href="<?php echo base_url() . 'fornecedor/editar/' . $fornec->id_fornecedor; ?>">
                                <img src="<?php echo base_url(); ?>assets/img/icon_edit.png" />
                            </a>
                        </td>
                        <td>
                            <a title="Deletar" href="<?php echo base_url() . 'fornecedor/deletar/' . $fornec->id_fornecedor; ?>" onclick="return confirm('Confirma a exclusão deste registro?')">
                                <img src="<?php echo base_url(); ?>assets/img/icon_delete.png" />
                            </a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</section>
<!-- Fim Lista -->
<?php include "templates/footer.php"; ?>