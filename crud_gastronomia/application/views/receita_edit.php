<?php include "templates/header.php"; ?>

<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">
        <?php echo form_open('receita/atualizar', 'id="form-receita"'); ?>

        <h1><?php echo $titulo ?></h1>
        <div class="form-group  label-floating">
            <input type="hidden" id="id-validado" name="id_receita" class="form-control" value="<?php echo $dados_receita[0]->id_receita; ?>"/>

            <label for="nome_receita" class="control-label">Nome da Receita:</label>
            <input type="text" name="nome_receita" class="form-control campo-validado" value="<?php echo $dados_receita[0]->nome_receita; ?>" maxlength="100" required/>
            <br/>
        </div>
        <div class="form-group  label-floating">
            <label for="observacao" class="control-label">Observação:</label>
            <input type="text" class="form-control" name="observacao" value="<?php echo $dados_receita[0]->observacao; ?>" maxlength="200" required/>
            <br/>
        </div>
        <div class="form-group  label-floating">
            <label for="modo_preparo" class="control-label">Modo de preparo:</label>
            <input type="text" name="modo_preparo" class="form-control" value="<?php echo $dados_receita[0]->modo_preparo; ?>" maxlength="999" required/>
            <br/>
        </div>
        <div class="form-group">
            <label for="receita_id_categoria_receita" >Categoria:</label>
            <select name="receita_id_categoria_receita"  class="form-control" >
                <?php foreach ($categoria_receita as $cat_receita): ?>
                    <option name="receita_id_categoria_receita" 
                            value="<?php echo $cat_receita->id_categoria_receita; ?>" <?php if ($cat_receita->id_categoria_receita === $dados_receita[0]->receita_id_categoria_receita) echo "selected" ?>>
                                <?php echo $cat_receita->nome_categoria_receita; ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>
        <br/>
        <table class="table text-center" id="insumos-receita">
            <thead>
                <tr>
                    <th class="text-center">Insumo</th>
                    <th class="text-center">Quantidade</th>
                    <th class="text-center">Medida</th>
                </tr>
            </thead>
            <tbody>
                <?php for ($i = 0; $i < count($insumos_relacionados); $i++) { ?>
                    <tr>
                        <td><?php echo $insumos_relacionados[$i]->nome_insumo; ?></td>
                        <td><input name="quantidade[]" type="number" value="<?php echo $insumos_relacionados[$i]->quantidade; ?>"/></td>
                        <td><?php echo $insumos_relacionados[$i]->nome_medida; ?></td>
                    </tr>
                <?php } ?>  
            </tbody>

            <?php for ($i = 0; $i < count($insumos_relacionados); $i++) { ?>
                <input type="hidden" name="id_insumo[]" value="<?php echo $insumos_relacionados[$i]->id_insumo; ?>">
                <input type="hidden" name="insumo_receita_id_receita[]" value="<?php echo $insumos_relacionados[$i]->insumo_receita_id_receita; ?>">
            <?php } ?>  
        </table>
        <input type="submit" class="btn btn-raised btn-primary"  name="atualizar" value="Atualizar" />

        <?php echo form_close(); ?>
    </div>

</main>

<?php include "templates/footer.php"; ?>