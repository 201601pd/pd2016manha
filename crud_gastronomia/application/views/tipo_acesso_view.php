<?php include "templates/header.php"; ?>

<main class="mdl-layout__content">

    <div class="mdl-layout__tab-panel is-active" id="overview">
        
        <?php echo form_open('tipo_acesso/inserir', 'id="form-tipo_acesso"'); ?>
        <h1><?php echo $titulo ?></h1>
        <div class="form-group label-floating">
            <label for="nome_tipo_acesso" class="control-label">Tipo de Acesso:</label>
            <input class="form-control campo-validado" type="text" name="nome_tipo_acesso" value="<?php echo set_value('nome_tipo_acesso'); ?>" maxlength="50" required/>
        </div>

        <div class="form-group label-floating">
            <label for="descricao" class="control-label">Descrição:</label>
            <input class="form-control" type="text" name="descricao" value="<?php echo set_value('descricao'); ?>" maxlength="200" required/>
        </div>
        </br>

        <input type="submit" class="btn btn-raised btn-primary" name="cadastrar" value="Cadastrar" />

        <?php echo form_close(); ?>

        <div class="sample">

            <!-- Lista as Pessoas Cadastradas -->

            <table class="table table-striped table-hover table-responsive">
                <thead>
                <th>Tipo de Acesso</th>
                <th>Editar</th>
                <th>Excluir</th>
                </thead>
                <tbody>                   
                    <?php foreach ($tipo_acesso as $tipo): ?>
                        <tr>
                            <td><?php echo $tipo->nome_tipo_acesso; ?></td>
                            <td>
                                <a title="Editar" href="<?php echo base_url() . 'tipo_acesso/editar/' . $tipo->id_tipo_acesso; ?>">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_edit.png" />
                                </a>
                            </td>
                            <td>
                                <a title="Deletar" href="<?php echo base_url() . 'tipo_acesso/deletar/' . $tipo->id_tipo_acesso; ?>" onclick="return confirm('Confirma a exclusão deste registro?')">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_delete.png" />
                                </a>
                            </td>                              
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- Fim Lista -->
</div>  <!-- Esse div pertence a algo do header?? -->
</main>
<?php include "templates/footer.php"; ?>