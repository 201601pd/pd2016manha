<?php include "templates/header.php"; ?>
<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">
        <?php echo form_open('associacao_aula_roteiro/inserir', 'id="form-associacao_aula_roteiro"'); ?>

        <h1><?php echo $titulo ?></h1>
        <div class="form-group label-floating">
            <label for="aula_roteiro_id_aula">Aula:</label><br/>
            <select class="form-control" name="aula_roteiro_id_aula">
                <option value="0" selected="true">Selecione...</option>
                <?php foreach ($aula as $au): ?>
                    <option value="<?php echo $au->id_aula; ?>"><?php echo $au->nome_aula; ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <div class="form-group label-floating">
            <label for="aula_roteiro_id_roteiro">Roteiro pertencente a aula:</label><br/>
            <select class="form-control" name="aula_roteiro_id_roteiro">
                <option value="0" selected="true">Selecione...</option>
                <?php foreach ($roteiro as $rot): ?>
                    <option value="<?php echo $rot->id_roteiro; ?>">
                        <?php echo $rot->titulo; ?> / 
                        <?php
                        foreach ($curso as $cur):
                            if ($rot->roteiro_id_curso === $cur->id_curso)
                                echo $cur->nome_curso;
                        endforeach;
                        ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>

        <div class="form-group label-floating">
            <label for="data_aula_roteiro">Data da aula:</label><br/>
            <input type="date" name="data_aula_roteiro" value="<?php echo set_value('data_aula_roteiro'); ?>" required/>
        </div>

        <input type="submit" class="btn btn-raised btn-primary" name="cadastrar" value="Cadastrar" />

        <?php echo form_close(); ?>

        <div class="sample">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                <th>Aula</th>
                <th>Roteiro</th>
                <th>Editar</th>
                <th>Excluir</th>
                </thead>  
                <tbody>
                    <?php foreach ($aula_roteiro as $au_rot): ?>
                        <tr>
                            <td>
                                <?php foreach ($aula as $au): ?>
                                    <?php if ($au->id_aula === $au_rot->aula_roteiro_id_aula) echo $au->nome_aula; ?>
                                <?php endforeach ?>
                            </td>
                            <td>
                                <?php foreach ($roteiro as $rot): ?>
                                    <?php
                                    if ($rot->id_roteiro === $au_rot->aula_roteiro_id_roteiro) {
                                        foreach ($curso as $cur):
                                            if ($rot->roteiro_id_curso === $cur->id_curso)
                                                echo $rot->titulo . ' - ' . $cur->nome_curso;
                                        endforeach;
                                    }
                                    ?>
                                <?php endforeach ?>
                            </td>    
                            <td>
                                <a title="Editar" href="<?php echo base_url() . 'associacao_aula_roteiro/editar/' . $au_rot->aula_roteiro_id_aula . '-' . $au_rot->aula_roteiro_id_roteiro; ?>">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_edit.png" />
                                </a>
                            </td>
                            <td>
                                <a title="Deletar" href="<?php echo base_url() . 'associacao_aula_roteiro/deletar/' . $au_rot->aula_roteiro_id_aula . '-' . $au_rot->aula_roteiro_id_roteiro; ?>" onclick="return confirm('Confirma a exclusão deste registro?')">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_delete.png" />
                                </a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</main>
<?php include "templates/footer.php"; ?>