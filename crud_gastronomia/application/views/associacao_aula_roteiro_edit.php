<?php include "templates/header.php"; ?>
<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">
        <?php echo form_open('associacao_aula_roteiro/atualizar', 'id="form-associacao_aula_roteiro"'); ?>

        <input type="hidden" name="old_aula_roteiro_id_aula" value="<?php echo $dados_associacao_aula_roteiro[0]->aula_roteiro_id_aula; ?>"/>
        <input type="hidden" name="old_aula_roteiro_id_roteiro" value="<?php echo $dados_associacao_aula_roteiro[0]->aula_roteiro_id_roteiro; ?>"/>

        <h1><?php echo $titulo ?></h1>

        <div class="form-group">
            <label for="aula_roteiro_id_aula">Aula:</label><br/>
            <select class="form-control" name="aula_roteiro_id_aula">
                <option value="0" selected="true">Selecione...</option>
                <?php foreach ($aula as $au): ?>
                    <option value="<?php echo $dados_associacao_aula_roteiro[0]->aula_roteiro_id_aula; ?>" 
                            <?php if ($au->id_aula === $dados_associacao_aula_roteiro[0]->aula_roteiro_id_aula) echo "selected" ?>><?php echo $au->nome_aula; ?></option>
                        <?php endforeach ?>
            </select>
            <div class="error"><?php echo form_error('aula_roteiro_id_aula'); ?></div> 
        </div>

        <div class="form-group">
            <label for="aula_roteiro_id_roteiro">Roteiro pertencente a aula:</label><br/>
            <select name="aula_roteiro_id_roteiro" class="form-control">
                <?php foreach ($roteiro as $rot): ?>
                    <option value="<?php echo $rot->id_roteiro; ?>" <?php if ($rot->id_roteiro === $dados_associacao_aula_roteiro[0]->aula_roteiro_id_roteiro) echo "selected" ?>>
                        <?php echo $rot->titulo; ?> / 
                        <?php
                        foreach ($curso as $cur):
                            if ($rot->roteiro_id_curso === $cur->id_curso)
                                echo $cur->nome_curso;
                        endforeach;
                        ?>
                    </option>
                <?php endforeach ?>
            </select>
            <div class="error"><?php echo form_error('aula_roteiro_id_roteiro'); ?></div>
        </div>

        <div class="form-group">
            <label class="control-label" for="data_aula_roteiro">Data da aula:</label><br/>
            <input class="form-control" type="date" name="data_aula_roteiro" value="<?php echo $dados_associacao_aula_roteiro[0]->data_aula_roteiro; ?>"/>
            <div class="error"><?php echo form_error('data_aula_roteiro'); ?></div>
        </div>

        <input type="submit" class="btn btn-raised btn-primary" name="atualizar" value="Atualizar" />

        <?php echo form_close(); ?>
    </div>
</main>
<?php include "templates/footer.php"; ?>