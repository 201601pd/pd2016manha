<?php include "templates/header.php"; ?>

<main class="mdl-layout__content">

    <div class="mdl-layout__tab-panel is-active" id="overview">

        <?php echo form_open('curso/inserir', 'id="form-curso"'); ?>
        <h1><?php echo $titulo ?></h1>
        <div class="form-group label-floating">
            <label for="nome_curso" class="control-label">Nome:</label>
            <input type="text" class="form-control campo-validado" name="nome_curso" value="<?php echo set_value('nome_curso'); ?>" maxlength="50" required/>
        </div>

        <div class="form-group label-floating">
            <label for="descricao" class="control-label">Descrição:</label>
            <input type="text" class="form-control" name="descricao" value="<?php echo set_value('descricao'); ?>" maxlength="200" required/>
        </div>
        </br>
        <input type="submit" class="btn btn-raised btn-primary" name="cadastrar" value="Cadastrar" />

        <?php echo form_close(); ?>

        <!-- Lista as Curso cadastradas -->
        <br><br>
        <div class="sample">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                <th>Curso</th>
                <th>Descrição</th>
                <th>Editar</th>
                <th>Excluir</th>
                </thead> 
                <tbody>
                    <?php foreach ($curso as $cur): ?>
                        <tr>
                            <td><?php echo $cur->nome_curso; ?></td>
                            <td><?php echo $cur->descricao; ?></td>
                            <td>
                                <a title="Editar" href="<?php echo base_url() . 'curso/editar/' . $cur->id_curso; ?>">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_edit.png" />
                                </a>
                            </td>
                            <td>
                                <a title="Deletar" href="<?php echo base_url() . 'curso/deletar/' . $cur->id_curso; ?>" onclick="return confirm('Confirma a exclusão deste registro?')">
                                    <img src="<?php echo base_url(); ?>assets/img/icon_delete.png" />
                                </a>
                            </td>   
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</main>
<?php include "templates/footer.php"; ?>