<?php include "templates/header.php"; ?>

<main class="mdl-layout__content">

    <div class="mdl-layout__tab-panel is-active" id="overview">

        <?php echo form_open('usuario/atualizar', 'id="form-usuario"'); ?>
        <h1><?php echo $titulo ?></h1>
        <input type="hidden" id="id-validado" name="id_usuario" value="<?php echo $dados_usuario[0]->id_usuario; ?>"/>

        <div class="form-group label-floating"> 
            <label for="nome_usuario" class="control-label">Nome:</label><br/>
            <input class="form-control" type="text" name="nome_usuario" value="<?php echo $dados_usuario[0]->nome_usuario; ?>" maxlength="50" required/>
        </div>

        <div class="form-group label-floating">
            <label for="matricula" class="control-label">Matrícula:</label><br/>
            <input class="form-control campo-validado" type="text" name="matricula" value="<?php echo $dados_usuario[0]->matricula; ?>" maxlength="50" required/>
        </div>

        <div class="form-group label-floating">
            <label for="senha" class="control-label">Senha:</label><br/>
            <input class="form-control" type="password" name="senha" value="<?php echo $dados_usuario[0]->senha; ?>" maxlength="50" required/>
        </div>

        <div class="form-group label-floating">
            <label for="email" class="control-label">E-mail:</label><br/>
            <input class="form-control" type="text" name="email" value="<?php echo $dados_usuario[0]->email; ?>" maxlength="70" required/>
        </div>

        <div class="form-group">
            <label for="usuario_id_tipo_acesso">Tipo de Acesso:</label><br/>
            <select name="usuario_id_tipo_acesso" class="form-control">
                <?php foreach ($tipo_acesso as $tipo): ?>
                    <option name="usuario_id_tipo_acesso" 
                            value="<?php echo $tipo->id_tipo_acesso; ?>" <?php if ($tipo->id_tipo_acesso === $dados_usuario[0]->usuario_id_tipo_acesso) echo "selected" ?>>
                                <?php echo $tipo->nome_tipo_acesso; ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>

        <input type="submit" class="btn btn-raised btn-primary" name="atualizar" value="Atualizar" />

        <?php echo form_close(); ?>
    </div>
</main>
<?php include "templates/footer.php"; ?>