<?php include "templates/header.php"; ?>
<main class="mdl-layout__content">
    <div class="mdl-layout__tab-panel is-active" id="overview">
        
        <?php echo form_open('categoria_insumo/atualizar', 'id="form-categoria_insumo"'); ?>
        <h1><?php echo $titulo ?></h1>
        <br>
        <input type="hidden" id="id-validado" name="id_categoria_insumo" value="<?php echo $dados_categoria_insumo[0]->id_categoria_insumo; ?>"/>
        
        <div class="form-group label-floating">
        <label for="nome_categoria_insumo" class="control-label">Nome:</label>
        <input type="text" class="form-control campo-validado" name="nome_categoria_insumo" value="<?php echo $dados_categoria_insumo[0]->nome_categoria_insumo; ?>" maxlength="50" required/>
        </div>
        
        <div class="error"><?php echo form_error('nome_categoria_insumo'); ?></div>

        <input type="submit" class="btn btn-raised btn-primary" name="atualizar" value="Atualizar" />

        <?php echo form_close(); ?>
        
    </div>
</main>
<?php include "templates/footer.php"; ?>