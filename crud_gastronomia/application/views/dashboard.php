<?php include "templates/header.php"; ?>

<section class="content">

    <div class="col-md-8" style="width: 50%">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Alertas de Vencimentos</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                            <tr>
                                <th>Data do Alerta</th>
                                <th>Insumo</th>
                                <th>Status</th>
                                <th>Data de Vencimento</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Massa</td>
                                <td><span class="label label-success">Vencimento em 30 dias</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#00a65a" data-height="20">29/06/2016</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Óleo de Soja</td>
                                <td><span class="label label-warning">Vencimento em 10 dias</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#f39c12" data-height="20">09/06/2016</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Açucar</td>
                                <td><span class="label label-danger">Vencido</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#f56954" data-height="20">29/05/2016</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Miojo</td>
                                <td><span class="label label-warning">Vencimento em 10 dias</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#00c0ef" data-height="20">09/06/2016</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Hot Pocket</td>
                                <td><span class="label label-warning">Vencimento em 10 dias</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#f39c12" data-height="20">09/06/2016</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Salsicha Enlatada</td>
                                <td><span class="label label-danger">Vencido</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#f56954" data-height="20">29/05/2016</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Ervilhas Enlatadas</td>
                                <td><span class="label label-success">Vencimento em 30 dias</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#00a65a" data-height="20">29/06/2016</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
        </div>


        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Solicitações em Aberto</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
        <!--                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                            <tr>
                                <th>Data do Alerta</th>
                                <th>Título</th>
                                <th>Aberta há</th>
                                <th>Data de Abertura</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Teste</td>
                                <td><span class="label label-success">10 dias</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#00a65a" data-height="20">29/06/2016</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Óleo de Soja</td>
                                <td><span class="label label-warning">30 dias</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#f39c12" data-height="20">09/06/2016</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Açucar</td>
                                <td><span class="label label-danger">40 dias</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#f56954" data-height="20">29/05/2016</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Miojo</td>
                                <td><span class="label label-warning">30 dias</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#00c0ef" data-height="20">09/06/2016</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Hot Pocket</td>
                                <td><span class="label label-warning">30 dias</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#f39c12" data-height="20">09/06/2016</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Ervilhas Enlatadas</td>
                                <td><span class="label label-success">10 dias</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#00a65a" data-height="20">29/06/2016</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
        </div>

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Alertas de Estoque</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
        <!--                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                            <tr>
                                <th>Data do Alerta</th>
                                <th>Insumo</th>
                                <th>Quantidade Atual</th>
                                <th>Quantidade Mínima</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Massa</td>
                                <td><span class="label label-success">10 Kg</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#00a65a" data-height="20">1 Kg</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Óleo de Soja</td>
                                <td><span class="label label-warning">10 L</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#f39c12" data-height="20">6 L</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Açucar</td>
                                <td><span class="label label-danger">5 Kg</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#f56954" data-height="20">5 Kg</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Miojo</td>
                                <td><span class="label label-warning">250 g</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#00c0ef" data-height="20">100 g</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Hot Pocket</td>
                                <td><span class="label label-warning">5 Unidades</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#f39c12" data-height="20">3 Unidades</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Salsicha Enlatada</td>
                                <td><span class="label label-danger">7 Latas</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#f56954" data-height="20">5 Latas</div>
                                </td>
                            </tr>
                            <tr>
                                <td>30/05/2016</td>
                                <td>Ervilhas Enlatadas</td>
                                <td><span class="label label-success">50 Latas</span></td>
                                <td>
                                    <div class="sparkbar" data-color="#00a65a" data-height="20">10 Latas</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
        </div>
    </div> 



    <div class="col-md-4" style="width: 50%">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Aulas da semana</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>

                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <ul class="products-list product-list-in-box">
                    <li class="item">
                        <div class="product-info">
                            <a href="javascript:void(0)" class="product-title">Segunda-feira (15/09/16)</a>
                            <span class="product-description">
                                Aula de introdução a molhos com base de tomate
                            </span>
                        </div>
                    </li>
                    <!-- /.item -->
                    <li class="item">
                        <div class="product-info">
                            <a href="javascript:void(0)" class="product-title">Terça-feira (16/09/16)</a>
                            <span class="product-description">
                                Cortes de carnes especiais
                            </span>
                        </div>
                    </li>
                    <!-- /.item -->
                    <li class="item">
                        <div class="product-info">
                            <a href="javascript:void(0)" class="product-title">Quarta-feira (17/09/16)</a>
                            <span class="product-description">
                                Massas e molhos - a arte de combiná-los
                            </span>
                        </div>
                    </li>
                    <!-- /.item -->
                    <li class="item">
                        <div class="product-info">
                            <a href="javascript:void(0)" class="product-title">Quinta-feira (18/09/16)</a>
                            <span class="product-description">
                                Harmonização de vinhos e queijos
                            </span>
                        </div>
                    </li>
                    <!-- /.item -->
                </ul>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="<?php echo base_url(); ?>calendario" class="uppercase">Ver todo o Calendário</a>
                </div>
            </div>
        </div>

        <!-- DONUT CHART -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Desperdícios</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="col-md-8">
                    <canvas id="pieChart" style="height:250px"></canvas>
                </div>
                <div class="col-md-2">
                    <ul class="chart-legend clearfix">
                        <li><i class="fa fa-circle-o text-red"></i> Atum</li>
                        <li><i class="fa fa-circle-o text-green"></i> Ervilha</li>
                        <li><i class="fa fa-circle-o text-yellow"></i> Salsicha</li>
                        <li><i class="fa fa-circle-o text-aqua"></i> Aipim</li>
                        <li><i class="fa fa-circle-o text-light-blue"></i> Morangos</li>
                        <li><i class="fa fa-circle-o text-gray"></i> Acelga</li>
                    </ul>

                </div>
            </div>
            <div class="box-footer text-center">
                <a href=".php" class="uppercase">Ver lista completa</a>
            </div>
        </div>
    </div>

</div>

</section>



<!-- /.box-footer -->

<?php include "templates/footer.php"; ?>

<!-- jQuery 2.2.0
<script src="../frontEnd/plugins/jQuery/jQuery-2.2.0.min.js"></script>
-->
<!-- ChartJS 1.0.1 -->
<script src="../frontEnd/plugins/chartjs/Chart.js"></script>

<!-- page script -->
<script>
    $(function () {


        //-------------
        //- PIE CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
        var pieChart = new Chart(pieChartCanvas);
        var PieData = [
            {
                value: 700,
                color: "#f56954",
                highlight: "#f56954",
                label: "Atum"
            },
            {
                value: 500,
                color: "#00a65a",
                highlight: "#00a65a",
                label: "Ervilha"
            },
            {
                value: 400,
                color: "#f39c12",
                highlight: "#f39c12",
                label: "Salsicha"
            },
            {
                value: 600,
                color: "#00c0ef",
                highlight: "#00c0ef",
                label: "Aipim"
            },
            {
                value: 300,
                color: "#3c8dbc",
                highlight: "#3c8dbc",
                label: "Morangos"
            },
            {
                value: 100,
                color: "#d2d6de",
                highlight: "#d2d6de",
                label: "Acelga"
            }
        ];
        var pieOptions = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke: true,
            //String - The colour of each segment stroke
            segmentStrokeColor: "#fff",
            //Number - The width of each segment stroke
            segmentStrokeWidth: 2,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 50, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps: 100,
            //String - Animation easing effect
            animationEasing: "easeOutBounce",
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        };
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        pieChart.Doughnut(PieData, pieOptions);

    });
</script>

