<?php include "templates/header.php"; ?>

<main class="mdl-layout__content">

    <div class="mdl-layout__tab-panel is-active" id="overview">

        <?php echo form_open('usuario/inserir', 'id="form-usuario"'); ?>
        <h1><?php echo $titulo ?></h1>
        <div class="form-group label-floating">
            <label for="nome_usuario" class="control-label">Nome:</label>
            <input class="form-control" type="text" name="nome_usuario" value="<?php echo set_value('nome_usuario'); ?>" maxlength="50" required/>
        </div>

        <div class="form-group label-floating">
            <label for="matricula" class="control-label">Matrícula:</label>
            <input class="form-control campo-validado" type="text" name="matricula" value="<?php echo set_value('matricula'); ?>" maxlength="50" required/>
        </div>

        <div class="form-group label-floating">
            <label for="senha" class="control-label">Senha:</label>
            <input class="form-control" type="password" name="senha" value="<?php echo set_value('senha'); ?>" maxlength="50" required/>
        </div>

        <div class="form-group label-floating">
            <label for="email" class="control-label">E-mail:</label>
            <input class="form-control" type="email" name="email" value="<?php echo set_value('email'); ?>" maxlength="70" required/>
        </div>

        <div class="form-group">
            <label for="usuario_id_tipo_acesso">Tipo de Acesso:</label><br/>
            <select class="form-control" name="usuario_id_tipo_acesso">
                <option value="0" selected="true">Selecione...</option>
                <?php foreach ($tipo_acesso as $tipo): ?>
                    <option value="<?php echo $tipo->id_tipo_acesso; ?>"><?php echo $tipo->nome_tipo_acesso; ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <input type="submit" class="btn btn-raised btn-primary" name="cadastrar" value="Cadastrar" />

        <?php echo form_close(); ?>

    </div>

</main>

<section class="listagem">

    <div class="sample">

        <!-- Lista as Pessoas Cadastradas -->
        <table class="table table-striped table-hover table-responsive">
            <thead>
            <th>Nome</th>
            <th>Matrícula</th>
            <th>E-mail</th>
            <th>Tipo de Acesso</th>
            <th>Editar</th>
            <th>Excluir</th>
            </thead>
            <tbody>
                <?php foreach ($usuario as $usu): ?>
                    <tr>
                        <td><?php echo $usu->nome_usuario; ?></td>
                        <td><?php echo $usu->matricula; ?></td>
                        <td><?php echo $usu->email; ?></td>
                        <td>
                            <?php foreach ($tipo_acesso as $tipo): ?>
                                <?php if ($tipo->id_tipo_acesso === $usu->usuario_id_tipo_acesso) echo $tipo->nome_tipo_acesso; ?>
                            <?php endforeach ?>
                        </td>
                        <td>
                            <a title="Editar" href="<?php echo base_url() . 'usuario/editar/' . $usu->id_usuario; ?>">
                                <img src="<?php echo base_url(); ?>assets/img/icon_edit.png" /> 
                            </a>
                        </td>
                        <td>
                            <a title="Deletar" href="<?php echo base_url() . 'usuario/deletar/' . $usu->id_usuario; ?>" onclick="return confirm('Confirma a exclusão deste registro?')">
                                <img src="<?php echo base_url(); ?>assets/img/icon_delete.png" />
                            </a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
        <!-- Fim Lista -->
    </div>
</section>
<?php include "templates/footer.php"; ?>