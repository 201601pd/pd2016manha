<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class insumo extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('insumo_model', 'model', TRUE);
        $this->load->model('medida_model', 'med_model', TRUE);
        $this->load->model('categoria_insumo_model', 'cat_model', TRUE);
        $this->load->model('fornecedor_model', 'forn_model', TRUE);
        $this->load->library('session');
        $this->load->library('validacao');
    }

    function index() {


        if ($this->validacao->check_session() == 1) {
            $this->load->helper('form');

            $session_data = $this->session->userdata('logged_in');

            $data['matricula'] = $session_data['matricula'];

            $data['titulo'] = "Cadastro de Insumos";
            $data['insumo'] = $this->model->listar();
            $data['medida'] = $this->med_model->listar();
            $data['categoria_insumo'] = $this->cat_model->listar();
            $data['fornecedor'] = $this->forn_model->listar();
            $this->load->view('insumo_view.php', $data);
        } else {
            redirect('verifica_login', 'refresh');
        }
    }

//index

    function inserir() {

        $data['nome_insumo'] = $this->input->post('nome_insumo');
        $data['qtd_minima'] = $this->input->post('qtd_minima');
        $data['qtd_atual'] = $this->input->post('qtd_atual');
        $data['perecivel'] = $this->input->post('perecivel');
        $data['insumo_id_medida'] = $this->input->post('insumo_id_medida');
        $data['insumo_id_categoria_insumo'] = $this->input->post('insumo_id_categoria_insumo');
        $data['insumo_id_fornecedor'] = $this->input->post('insumo_id_fornecedor');

        /* Chama a função inserir do modelo */
        if ($this->model->inserir($data)) {
            redirect('insumo');
        } else {
            log_message('error', 'Erro ao inserir o insumo.');
        }
    }

    function editar($id_insumo) {
        /* Aqui vamos definir o título da página de edição */
        $data['titulo'] = "Editar Insumos | AppGastronomia";

        $data['medida'] = $this->med_model->listar();
        $data['categoria_insumo'] = $this->cat_model->listar();
        $data['fornecedor'] = $this->forn_model->listar();
        /* Busca os dados da pessoa que será editada */
        $data['dados_insumo'] = $this->model->editar($id_insumo);

        /* Carrega a página de edição com os dados da pessoa */
        $this->load->view('insumo_edit', $data);
    }

    function atualizar() {
        $data['id_insumo'] = $this->input->post('id_insumo');
        $data['nome_insumo'] = $this->input->post('nome_insumo');
        $data['qtd_minima'] = $this->input->post('qtd_minima');
        $data['qtd_atual'] = $this->input->post('qtd_atual');
        $data['perecivel'] = $this->input->post('perecivel');
        $data['insumo_id_medida'] = $this->input->post('insumo_id_medida');
        $data['insumo_id_categoria_insumo'] = $this->input->post('insumo_id_categoria_insumo');
        $data['insumo_id_fornecedor'] = $this->input->post('insumo_id_fornecedor');

        /* Executa a função atualizar do modelo passando como parâmetro os dados obtidos do formulário */
        if ($this->model->atualizar($data)) {
            redirect('insumo');
        } else {
            log_message('error', 'Erro ao atualizar o insumo.');
        }
    }

    function deletar($id_insumo) {
        /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
        if ($this->model->deletar($id_insumo)) {
            redirect('insumo');
        } else {
            log_message('error', 'Erro ao deletar o insumo.');
        }
    }

}
