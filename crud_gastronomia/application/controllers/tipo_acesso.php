<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class tipo_acesso extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('tipo_acesso_model', 'model', TRUE);
        $this->load->library('session');
        $this->load->library('validacao');
    }

    function index() {
//         if ($this->validacao->check_session() == 1) {
            $this->load->helper('form');

//            $session_data = $this->session->userdata('logged_in');
            
            $data['titulo'] = "Cadastro de Tipos de Acesso";
            $data['tipo_acesso'] = $this->model->listar();
            $this->load->view('tipo_acesso_view.php', $data);
//        } else {
//            redirect('verifica_login', 'refresh');
//        }
    }
    
    function getCamposCadastrados() {
        $objetos_cadastrados = $this->model->listar();

        $arr_campos_cadastrados = array();
        foreach ($objetos_cadastrados as $obj) {
            $arr_campos_cadastrados[] = '{"campo_validado":"' . $obj->nome_tipo_acesso . '"}';
        }
        echo '[ ' . implode(",", $arr_campos_cadastrados) . ']';

        return;
    }

    function getCampoEditado($id_obj) {
        $objeto = $this->model->editar($id_obj);
        echo '[{"id":' . $objeto[0]->id_tipo_acesso . ',"campo_validado":"' . $objeto[0]->nome_tipo_acesso . '"}]';
        return;
    }
    
    function inserir() {
        /* Recebe os dados do formulário (visão) */
        $data['nome_tipo_acesso'] = $this->input->post('nome_tipo_acesso');
        $data['descricao'] = $this->input->post('descricao');

        /* Chama a função inserir do modelo */
        if ($this->model->inserir($data)) {
            redirect('tipo_acesso');
        } else {
            log_message('error', 'Erro ao inserir o roteiro.');
        }
    }

    function editar($id_tipo_acesso) {
        /* Aqui vamos definir o título da página de edição */
        $data['titulo'] = "Editar Tipos de Acesso | AppGastronomia";

        /* Busca os dados da pessoa que será editada */
        $data['dados_tipo_acesso'] = $this->model->editar($id_tipo_acesso);

        /* Carrega a página de edição com os dados da pessoa */
        $this->load->view('tipo_acesso_edit', $data);
    }

    function atualizar() {
        /* Senão obtém os dados do formulário */
        $data['id_tipo_acesso'] = $this->input->post('id_tipo_acesso');
        $data['nome_tipo_acesso'] = $this->input->post('nome_tipo_acesso');
        $data['descricao'] = $this->input->post('descricao');

        /* Executa a função atualizar do modelo passando como parâmetro os dados obtidos do formulário */
        if ($this->model->atualizar($data)) {
            redirect('tipo_acesso');
        } else {
            log_message('error', 'Erro ao inserir o tipo de acesso.');
        }
    }

    function deletar($id_tipo_acesso) {
        /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
        if ($this->model->deletar($id_tipo_acesso)) {
            redirect('tipo_acesso');
        } else {
            log_message('error', 'Erro ao deletar o tipo de acesso.');
        }
    }

}
