<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class calendario extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('validacao');
    }

    function index() {
        if ($this->validacao->check_session() == 1 || $this->validacao->check_session() == 2) {
            $this->load->helper('form');

            $session_data = $this->session->userdata('logged_in');

            $data['matricula'] = $session_data['matricula'];

            $data['titulo'] = "Calendário";
            $this->load->helper(array('form'));
            $this->load->view('calendario', $data);
        } else {
            redirect('verifica_login', 'refresh');
        }
    }

}
