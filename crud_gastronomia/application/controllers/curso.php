<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class curso extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('curso_model', 'model', TRUE);
        $this->load->library('session');
        $this->load->library('validacao');
    }

    function index() {
        if ($this->validacao->check_session() == 1) {
            $this->load->helper('form');

            $session_data = $this->session->userdata('logged_in');

            $data['titulo'] = "Cadastro de Cursos";
            $data['curso'] = $this->model->listar();
            $this->load->view('curso_view.php', $data);
        } else {
            redirect('verifica_login', 'refresh');
        }
    }

    function getCamposCadastrados() {
        $objetos_cadastrados = $this->model->listar();

        $arr_campos_cadastrados = array();
        foreach ($objetos_cadastrados as $obj) {
            $arr_campos_cadastrados[] = '{"campo_validado":"' . $obj->nome_curso . '"}';
        }
        echo '[ ' . implode(",", $arr_campos_cadastrados) . ']';

        return;
    }

    function getCampoEditado($id_obj) {
        $objeto = $this->model->editar($id_obj);
        echo '[{"id":' . $objeto[0]->id_curso . ',"campo_validado":"' . $objeto[0]->nome_curso . '"}]';
        return;
    }

    function inserir() {

        /* Recebe os dados do formulário (visão) */
        $data['nome_curso'] = $this->input->post('nome_curso');
        $data['descricao'] = $this->input->post('descricao');

        /* Chama a função inserir do modelo */
        if ($this->model->inserir($data)) {
            redirect('curso');
        } else {
            log_message('error', 'Erro ao inserir o curso.');
        }
    }

    function editar($id_curso) {
        /* Aqui vamos definir o título da página de edição */
        $data['titulo'] = "Editar Cursos | AppGastronomia";

        /* Busca os dados da pessoa que será editada */
        $data['dados_curso'] = $this->model->editar($id_curso);

        /* Carrega a página de edição com os dados da pessoa */
        $this->load->view('curso_edit', $data);
    }

    function atualizar() {
        /* Senão obtém os dados do formulário */
        $data['id_curso'] = $this->input->post('id_curso');
        $data['nome_curso'] = $this->input->post('nome_curso');
        $data['descricao'] = $this->input->post('descricao');

        /* Executa a função atualizar do modelo passando como parâmetro os dados obtidos do formulário */
        if ($this->model->atualizar($data)) {
            redirect('curso');
        } else {
            log_message('error', 'Erro ao inserir o curso.');
        }
    }

    function deletar($id_curso) {
        /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
        if ($this->model->deletar($id_curso)) {
            redirect('curso');
        } else {
            log_message('error', 'Erro ao inserir o curso.');
        }
    }

}
