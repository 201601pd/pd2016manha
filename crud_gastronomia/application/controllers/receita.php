<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class receita extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('receita_model', 'model', TRUE);
        $this->load->model('categoria_receita_model', 'cat_model', TRUE);
        $this->load->model('insumo_model', 'ins_model', TRUE);
        $this->load->model('insumo_receita_model', 'insrec_model', TRUE);
        $this->load->model('medida_model', 'med_model', TRUE);
        $this->load->library('session');
        $this->load->library('validacao');
    }

    function index() {
        if ($this->validacao->check_session() == 1 || $this->validacao->check_session() == 2) {
        $this->load->helper('form');
//
          $session_data = $this->session->userdata('logged_in');

        $data['titulo'] = "Cadastro de Receitas | AppGastronomia";
        $data['receita'] = $this->model->listar();
        $data['categoria_receita'] = $this->cat_model->listar();
        $data['insumo'] = $this->ins_model->listar();
        $data['insumo_receita'] = $this->insrec_model->listar();
        $data['medida'] = $this->med_model->listar();
        $data['proximo_id'] = $this->model->getNextId();
        $this->load->view('receita_view.php', $data);
        } else {
          redirect('verifica_login', 'refresh');
        }
    }

    function getInsumosDaReceita($id_receita) {
        $insumosRelacionados = $this->model->getInsumosRelacionados($id_receita);

        $arr_insumos_relacionados = array();
        foreach ($insumosRelacionados as $insumo) {
            $arr_insumos_relacionados[] = '{"id_insumo":' . $insumo->id_insumo . ','
                    . '"nome_insumo":"' . $insumo->nome_insumo . '",'
                    . '"quantidade":' . $insumo->quantidade . ','
                    . '"nome_medida":"' . $insumo->nome_medida . '"}';
        }
        echo '[ ' . implode(",", $arr_insumos_relacionados) . ']';
    }

    function getCamposCadastrados() {
        $objetos_cadastrados = $this->model->listar();

        $arr_campos_cadastrados = array();
        foreach ($objetos_cadastrados as $obj) {
            $arr_campos_cadastrados[] = '{"campo_validado":"' . $obj->nome_receita . '"}';
        }
        echo '[ ' . implode(",", $arr_campos_cadastrados) . ']';

        return;
    }

    function getCampoEditado($id_obj) {
        $objeto = $this->model->editar($id_obj);
        echo '[{"id":' . $objeto[0]->id_receita . ',"campo_validado":"' . $objeto[0]->nome_receita . '"}]';
        return;
    }

    public function json_insumos() {
        $query = $this->ins_model->listar();
        $query_medidas = $this->med_model->listar();
        $json_data = array();
        foreach ($query as $key => $value) {
            foreach ($query_medidas as $key_medida => $value_medida) {
                if ($value->insumo_id_medida === $value_medida->id_medida) {
                    $json_data[] = array('id' => $value->id_insumo, 'nome' => $value->nome_insumo, 'medida' => $value_medida->nome_medida);
                }
            }
        }
        echo json_encode($json_data);
    }

    function inserir() {
        $data['nome_receita'] = $this->input->post('nome_receita');
        $data['observacao'] = $this->input->post('observacao');
        $data['modo_preparo'] = $this->input->post('modo_preparo');
        $data['receita_id_categoria_receita'] = $this->input->post('receita_id_categoria_receita');

        $this->model->inserir($data);
        //Inicia a inserção dos insumo_receita
        $arrayInsumos = $this->input->post('insumos');
        $arrayQuantidades = $this->input->post('quantidades');
        $idReceita = $this->input->post('insumo_receita_id_receita');
        for ($i = 0; $i < count($arrayInsumos); $i++) {
            $data2 = array('insumo_receita_id_insumo' => $arrayInsumos[$i],
                'insumo_receita_id_receita' => $idReceita,
                'quantidade' => $arrayQuantidades[$i]);
            $this->insrec_model->inserir($data2);
        }

        redirect('receita');
        /* Chama a função inserir do modelo */
//        if ($this->model->inserir($data)) {
//            redirect('receita');
//        } else {
//            log_message('error', 'Erro ao inserir a receita.');
//        }
    }

    //Falta fazer o editar
    function editar($id_receita) {
        /* Aqui vamos definir o título da página de edição */
        $data['titulo'] = "Editar Receita";

        $data['categoria_receita'] = $this->cat_model->listar();
        $data['insumos_relacionados'] = $this->model->getInsumosRelacionados($id_receita);

        /* Busca os dados da receita que será editada */
        $data['dados_receita'] = $this->model->editar($id_receita);

        /* Carrega a página de edição com os dados da pessoa */
        $this->load->view('receita_edit', $data);
    }

    function atualizar() {
        $data['id_receita'] = $this->input->post('id_receita');
        $data['nome_receita'] = $this->input->post('nome_receita');
        $data['observacao'] = $this->input->post('observacao');
        $data['modo_preparo'] = $this->input->post('modo_preparo');
        $data['receita_id_categoria_receita'] = $this->input->post('receita_id_categoria_receita');

        $dataInsumos['quantidades'] = $this->input->post('quantidade[]');
        $dataInsumos['insumos_receita_id_receita'] = $this->input->post('insumo_receita_id_receita[]');
        $dataInsumos['id_insumos'] = $this->input->post('id_insumo[]');

        for ($i = 0; $i < count($dataInsumos['quantidades']); $i++) {
            $novaQuantidade = $dataInsumos['quantidades'][$i];
            $idInsumo = $dataInsumos['id_insumos'][$i];
            $idReceita = $dataInsumos['insumos_receita_id_receita'][$i];
            
            if($this->model->atualizarInsumosRelacionados($novaQuantidade, $idInsumo, $idReceita)){
                //Necessita a verificação para saber se a receita e os insumos poderão ser atualizados
            }
        }

        /* Executa a função atualizar do modelo passando como parâmetro os dados obtidos do formulário */
        if ($this->model->atualizar($data)) {
            redirect('receita');
        } else {
            log_message('error', 'Erro ao inserir o receita.');
        }
    }

    function deletar($id_receita) {
        /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
        if ($this->model->deletar($id_receita)) {
            redirect('receita');
        } else {
            log_message('error', 'Erro ao inserir o receita.');
        }
    }

}
