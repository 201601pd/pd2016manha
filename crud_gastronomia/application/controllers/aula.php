<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class aula extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('aula_model', 'model', TRUE);
        $this->load->library('session');
        $this->load->library('validacao');
    }

    function index() {
        if ($this->validacao->check_session() == 1 || $this->validacao->check_session() == 2) {
            $this->load->helper('form');

            $session_data = $this->session->userdata('logged_in');

            $data['titulo'] = "Cadastro de Aulas";
            $data['aula'] = $this->model->listar();
            $this->load->view('aula_view.php', $data);
        } else {
            redirect('verifica_login', 'refresh');
        }
    }

    function getCamposCadastrados() {
        $objetos_cadastrados = $this->model->listar();

        $arr_campos_cadastrados = array();
        foreach ($objetos_cadastrados as $obj) {
            $arr_campos_cadastrados[] = '{"campo_validado":"' . $obj->nome_aula . '"}';
        }
        echo '[ ' . implode(",", $arr_campos_cadastrados) . ']';

        return;
    }

    function getCampoEditado($id_obj) {
        $objeto = $this->model->editar($id_obj);
        echo '[{"id":' . $objeto[0]->id_aula . ',"campo_validado":"' . $objeto[0]->nome_aula . '"}]';
        return;
    }

    function inserir() {
        /* Recebe os dados do formulário (visão) */
        $data['nome_aula'] = $this->input->post('nome_aula');

        /* Chama a função inserir do modelo */
        if ($this->model->inserir($data)) {
            redirect('aula');
        } else {
            log_message('error', 'Erro ao inserir a aula.');
        }
    }

    function editar($id_aula) {
        /* Aqui vamos definir o título da página de edição */
        $data['titulo'] = "Editar Aula | AppGastronomia";

        /* Busca os dados da pessoa que será editada */
        $data['dados_aula'] = $this->model->editar($id_aula);

        /* Carrega a página de edição com os dados da pessoa */
        $this->load->view('aula_edit', $data);
    }

    function atualizar() {
        /* Senão obtém os dados do formulário */
        $data['id_aula'] = $this->input->post('id_aula');
        $data['nome_aula'] = $this->input->post('nome_aula');

        /* Executa a função atualizar do modelo passando como parâmetro os dados obtidos do formulário */
        if ($this->model->atualizar($data)) {
            redirect('aula');
        } else {
            log_message('error', 'Erro ao editar a aula.');
        }
    }

    function deletar($id_aula) {
        /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
        if ($this->model->deletar($id_aula)) {
            redirect('aula');
        } else {
            log_message('error', 'Erro ao deletar a aula.');
        }
    }

}
