<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class associacao_aula_roteiro extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('aula_roteiro_model', 'model', TRUE);
        $this->load->model('aula_model', 'au_model', TRUE);
        $this->load->model('roteiro_model', 'rot_model', TRUE);
        $this->load->model('curso_model', 'cur_model', TRUE);
        $this->load->library('session');
        $this->load->library('validacao');
    }

    function index() {
        if ($this->validacao->check_session() == 1 || $this->validacao->check_session() == 2) {
            $this->load->helper('form');

            $session_data = $this->session->userdata('logged_in');

            $data['titulo'] = "Associações de Aulas e Roteiros";
            $data['aula_roteiro'] = $this->model->listar();
            $data['aula'] = $this->au_model->listar();
            $data['roteiro'] = $this->rot_model->listar();
            $data['curso'] = $this->cur_model->listar();
            $this->load->view('associacao_aula_roteiro_view.php', $data);
        } else {
            redirect('verifica_login', 'refresh');
        }
    }

    function inserir() {
        /* Recebe os dados do formulário (visão) */
        $data['aula_roteiro_id_aula'] = $this->input->post('aula_roteiro_id_aula');
        $data['aula_roteiro_id_roteiro'] = $this->input->post('aula_roteiro_id_roteiro');
        $data['data_aula_roteiro'] = $this->input->post('data_aula_roteiro');

        /* Chama a função inserir do modelo */
        if ($this->model->inserir($data)) {
            redirect('associacao_aula_roteiro');
        } else {
            log_message('error', 'Erro ao inserir o roteiro.');
        }
    }

    function editar($ids_aula_roteiro) {
        /* Aqui vamos definir o título da página de edição */
        $data['titulo'] = "Editar Associação | AppGastronomia";

        $data['aula'] = $this->au_model->listar();
        $data['roteiro'] = $this->rot_model->listar();
        $data['curso'] = $this->cur_model->listar();
        /* Busca os dados da pessoa que será editada */
        $data['dados_associacao_aula_roteiro'] = $this->model->editar($ids_aula_roteiro);

        /* Carrega a página de edição com os dados da pessoa */
        $this->load->view('associacao_aula_roteiro_edit', $data);
    }

    function atualizar() {
        /* Senão obtém os dados do formulário */
        $data['aula_roteiro_id_aula'] = $this->input->post('aula_roteiro_id_aula');
        $data['aula_roteiro_id_roteiro'] = $this->input->post('aula_roteiro_id_roteiro');
        $data['data_aula_roteiro'] = $this->input->post('data_aula_roteiro');
        $auxData['old_aula_roteiro_id_aula'] = $this->input->post('old_aula_roteiro_id_aula');
        $auxData['old_aula_roteiro_id_roteiro'] = $this->input->post('old_aula_roteiro_id_roteiro');

        /* Executa a função atualizar do modelo passando como parâmetro os dados obtidos do formulário */
        if ($this->model->atualizar($data, $auxData)) {
            redirect('associacao_aula_roteiro');
        } else {
            log_message('error', 'Erro ao atualizar a associação.');
        }
    }

    function deletar($ids_aula_roteiro) {
        /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
        if ($this->model->deletar($ids_aula_roteiro)) {
            redirect('associacao_aula_roteiro');
        } else {
            log_message('error', 'Erro ao deletar a associação.');
        }
    }

}
