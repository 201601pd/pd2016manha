<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class associacao_aula_receita extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('aula_receita_model', 'model', TRUE);
        $this->load->model('aula_model', 'au_model', TRUE);
        $this->load->model('receita_model', 'rec_model', TRUE);
        $this->load->library('session');
        $this->load->library('validacao');
    }

    function index() {
        if ($this->validacao->check_session() == 1 || $this->validacao->check_session() == 2) {
            $this->load->helper('form');

            $session_data = $this->session->userdata('logged_in');

            $data['titulo'] = "Associações de Aulas e Receitas";
            $data['aula_receita'] = $this->model->listar();
            $data['aula'] = $this->au_model->listar();
            $data['receita'] = $this->rec_model->listar();
            $this->load->view('associacao_aula_receita_view.php', $data);
        } else {
            redirect('verifica_login', 'refresh');
        }
    }

    function inserir() {
        $data['aula_receita_id_aula'] = $this->input->post('aula_receita_id_aula');
        $data['aula_receita_id_receita'] = $this->input->post('aula_receita_id_receita');

        if ($this->model->inserir($data)) {
            redirect('associacao_aula_receita');
        } else {
            log_message('error', 'Erro ao inserir o roteiro.');
        }
    }

    function editar($ids_aula_receita) {
        $data['titulo'] = "Editar Associação | AppGastronomia";

        $data['aula'] = $this->au_model->listar();
        $data['receita'] = $this->rec_model->listar();
        /* Busca os dados da pessoa que será editada */
        $data['dados_associacao_aula_receita'] = $this->model->editar($ids_aula_receita);

        /* Carrega a página de edição com os dados da pessoa */
        $this->load->view('associacao_aula_receita_edit', $data);
    }

    function atualizar() {
        /* Senão obtém os dados do formulário */
        $data['aula_receita_id_aula'] = $this->input->post('aula_receita_id_aula');
        $data['aula_receita_id_receita'] = $this->input->post('aula_receita_id_receita');
        $auxData['old_aula_receita_id_aula'] = $this->input->post('old_aula_receita_id_aula');
        $auxData['old_aula_receita_id_receita'] = $this->input->post('old_aula_receita_id_receita');

        /* Executa a função atualizar do modelo passando como parâmetro os dados obtidos do formulário */
        if ($this->model->atualizar($data, $auxData)) {
            redirect('associacao_aula_receita');
        } else {
            log_message('error', 'Erro ao atualizar a associação.');
        }
    }

    function deletar($ids_aula_roteiro) {
        /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
        if ($this->model->deletar($ids_aula_roteiro)) {
            redirect('associacao_aula_receita');
        } else {
            log_message('error', 'Erro ao deletar a associação.');
        }
    }

}
