<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class medida extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('medida_model', 'model', TRUE);
        $this->load->library('session');
        $this->load->library('validacao');
    }

    function index() {
        if ($this->validacao->check_session() == 1) {
            $this->load->helper('form');

            $session_data = $this->session->userdata('logged_in');

            $data['titulo'] = "Cadastro de Medidas";
            $data['medida'] = $this->model->listar();
            $this->load->view('medida_view.php', $data);
        } else {
            redirect('verifica_login', 'refresh');
        }
    }

    function getCamposCadastrados() {
        $objetos_cadastrados = $this->model->listar();

        $arr_campos_cadastrados = array();
        foreach ($objetos_cadastrados as $obj) {
            $arr_campos_cadastrados[] = '{"campo_validado":"' . $obj->nome_medida . '"}';
        }
        echo '[ ' . implode(",", $arr_campos_cadastrados) . ']';

        return;
    }

    function getCampoEditado($id_obj) {
        $objeto = $this->model->editar($id_obj);
        echo '[{"id":' . $objeto[0]->id_medida . ',"campo_validado":"' . $objeto[0]->nome_medida . '"}]';
        return;
    }

    function inserir() {
        /* Recebe os dados do formulário (visão) */
        $data['nome_medida'] = $this->input->post('nome_medida');

        /* Chama a função inserir do modelo */
        if ($this->model->inserir($data)) {
            redirect('medida');
        } else {
            log_message('error', 'Erro ao inserir a medida.');
        }
    }

    function editar($id_medida) {
        /* Aqui vamos definir o título da página de edição */
        $data['titulo'] = "Editar Medidas | AppGastronomia";

        /* Busca os dados da pessoa que será editada */
        $data['dados_medida'] = $this->model->editar($id_medida);

        /* Carrega a página de edição com os dados da pessoa */
        $this->load->view('medida_edit', $data);
    }

    function atualizar() {
        /* Senão obtém os dados do formulário */
        $data['id_medida'] = $this->input->post('id_medida');
        $data['nome_medida'] = $this->input->post('nome_medida');

        /* Executa a função atualizar do modelo passando como parâmetro os dados obtidos do formulário */
        if ($this->model->atualizar($data)) {
            redirect('medida');
        } else {
            log_message('error', 'Erro ao atualizar a medida.');
        }
    }

    function deletar($id_medidas) {
        /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
        if ($this->model->deletar($id_medidas)) {
            redirect('medida');
        } else {
            log_message('error', 'Erro ao deletar a medida.');
        }
    }

}
