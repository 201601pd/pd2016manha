<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class categoria_insumo extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('categoria_insumo_model', 'model', TRUE);
        $this->load->library('session');
        $this->load->library('validacao');
    }

    function index() {
        if ($this->validacao->check_session() == 1) {
            $this->load->helper('form');

            $session_data = $this->session->userdata('logged_in');

            $data['titulo'] = "Cadastro de Categoria de Insumo";
            $data['categoria_insumo'] = $this->model->listar();
            $this->load->view('categoria_insumo_view.php', $data);
        } else {
            redirect('verifica_login', 'refresh');
        }
    }

    function getCamposCadastrados() {
        $objetos_cadastrados = $this->model->listar();

        $arr_campos_cadastrados = array();
        foreach ($objetos_cadastrados as $obj) {
            $arr_campos_cadastrados[] = '{"campo_validado":"' . $obj->nome_categoria_insumo . '"}';
        }
        echo '[ ' . implode(",", $arr_campos_cadastrados) . ']';

        return;
    }

    function getCampoEditado($id_obj) {
        $objeto = $this->model->editar($id_obj);
        echo '[{"id":' . $objeto[0]->id_categoria_insumo . ',"campo_validado":"' . $objeto[0]->nome_categoria_insumo . '"}]';
        return;
    }

    function inserir() {

        /* Recebe os dados do formulário (visão) */
        $data['nome_categoria_insumo'] = $this->input->post('nome_categoria_insumo');

        /* Chama a função inserir do modelo */
        if ($this->model->inserir($data)) {
            redirect('categoria_insumo');
        } else {
            log_message('error', 'Erro ao inserir a categoria do insumo.');
        }
    }

    function editar($id_categoria_insumo) {
        /* Aqui vamos definir o título da página de edição */
        $data['titulo'] = "Editar Categoria de Insumo | AppGastronomia";

        /* Busca os dados da pessoa que será editada */
        $data['dados_categoria_insumo'] = $this->model->editar($id_categoria_insumo);

        /* Carrega a página de edição com os dados da pessoa */
        $this->load->view('categoria_insumo_edit', $data);
    }

    function atualizar() {
        /* Senão obtém os dados do formulário */
        $data['id_categoria_insumo'] = $this->input->post('id_categoria_insumo');
        $data['nome_categoria_insumo'] = $this->input->post('nome_categoria_insumo');

        /* Executa a função atualizar do modelo passando como parâmetro os dados obtidos do formulário */
        if ($this->model->atualizar($data)) {
            redirect('categoria_insumo');
        } else {
            log_message('error', 'Erro ao atualizar a categoria do insumo.');
        }
    }

    function deletar($id_categoria_insumo) {
        /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
        if ($this->model->deletar($id_categoria_insumo)) {
            redirect('categoria_insumo');
        } else {
            log_message('error', 'Erro ao atualizar a categoria do insumo.');
        }
    }

}
