<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class solicitacao extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('solicitacao_model', 'model', TRUE);
        $this->load->model('curso_model', 'cur_model', TRUE);
        $this->load->model('roteiro_model', 'rot_model', TRUE);
        $this->load->model('aula_model', 'au_model', TRUE);
        $this->load->model('fornecedor_model', 'for_model', TRUE);
        $this->load->model('solicitacao_insumo_model', 'sol_ins_model', TRUE);
        $this->load->library('session');
        $this->load->library('validacao');
    }

    function index() {
        if ($this->validacao->check_session() == 1) {
            $this->load->helper('form');

            $session_data = $this->session->userdata('logged_in');

            $data['titulo'] = "Gerar Requisição";
            $data['curso'] = $this->cur_model->listar();
            $data['roteiro'] = $this->rot_model->listar();
            $data['aula'] = $this->au_model->listar();
            $this->load->view('solicitacao_view.php', $data);
        } else {
            redirect('verifica_login', 'refresh');
        }
    }

    function getRoteiros($id) {
        $roteiros = $this->rot_model->getRoteirosById($id);

        if (empty($roteiros))
            return '{ "titulo": "Nenhum roteiro encontrado" }';

        $arr_roteiro = array();
        foreach ($roteiros as $roteiro) {
            $arr_roteiro[] = '{"id_roteiro":' . $roteiro->id_roteiro . ',"titulo":"' . $roteiro->titulo . '"}';
        }
        echo '[ ' . implode(",", $arr_roteiro) . ']';

        return;
    }

    function getAulas($id) {
        $aulas = $this->au_model->getAulasById($id);

        if (empty($aulas))
            return '{ "nome_aula": "Nenhum roteiro encontrado" }';

        $arr_aulas = array();
        foreach ($aulas as $aula) {
            $arr_aulas[] = '{"id_aula":' . $aula->id_aula . ',"nome_aula":"' . $aula->nome_aula . '"}';
        }
        echo '[ ' . implode(",", $arr_aulas) . ']';

        return;
    }

    function getInsumos($ids) {
        $insumos = $this->model->getInsumosByAulaId($ids);

        if (empty($insumos)) {
            return '{ "insumos": "Nenhum roteiro encontrado" }';
        }

        $arr_insumos = array();
        foreach ($insumos as $insumo) {
            $arr_insumos[] = '{"id_insumo":' . $insumo->id_insumo . ','
                    . '"nome_insumo":"' . $insumo->nome_insumo . '",'
                    . '"qtd_atual":' . $insumo->qtd_atual . ','
                    . '"nome_medida":"' . $insumo->nome_medida . '",'
                    . '"qtd_total": ' . $insumo->qtd_total . ','
                    . '"insumo_id_fornecedor":' . $insumo->insumo_id_fornecedor . '}';
        }
        echo '[ ' . implode(",", $arr_insumos) . ']';

        return;
    }

    function gerar() {
        $data['curso'] = $this->input->post('curso');
        $data['roteiro'] = $this->input->post('roteiro');
        $data['insumos_ids'] = $this->input->post('insumos[]');
        $data['fornecedores'] = $this->input->post('fornecedores[]');
        $data['quantidades'] = $this->input->post('insumos-quantidade[]');
        $data['observacoes'] = $this->input->post('insumos-observacao[]');

        $fornecedoresAtivos = $this->for_model->listarAtivos();
        $solicitacoes = array();

        for ($i = 0; $i < count($fornecedoresAtivos); $i++) {
            for ($j = 0; $j < count($data['fornecedores']); $j++) {
                if ($fornecedoresAtivos[$i]->id_fornecedor == $data['fornecedores'][$j]) {
                    $solicitacoes[] = (object) ["id_solicitacao" => $this->model->getNextId()[0]->auto_increment,
                                "id_fornecedor" => $data['fornecedores'][$j]];
                    $solicitacao['solicitacao_id_fornecedor'] = $data['fornecedores'][$j];
                    $solicitacao['status_solicitacao'] = true;

                    $this->model->inserir($solicitacao);
                    break;
                }
            }
        }

        for ($i = 0; $i < count($solicitacoes); $i++) {
            for ($j = 0; $j < count($data['insumos_ids']); $j++) {
                if ($solicitacoes[$i]->id_fornecedor == $data['fornecedores'][$j]) {
                    $solicitacao_insumo['solicitacao_insumo_id_insumo'] = $data['insumos_ids'][$j];
                    $solicitacao_insumo['solicitacao_insumo_id_solicitacao'] = $solicitacoes[$i]->id_solicitacao;
                    $solicitacao_insumo['quantidade'] = $data['quantidades'][$j];
                    $solicitacao_insumo['observacao'] = $data['observacoes'][$j];

                    $this->sol_ins_model->inserir($solicitacao_insumo);
                }
            }
        }

        $solicitacoes_geradas = array();
        for ($i = 0; $i < count($solicitacoes); $i++) {
            $solicitacoes_geradas[] = $this->sol_ins_model->retornaInsumos($solicitacoes[$i]->id_solicitacao);
        }
        $dados['solicitacoes_geradas'] = $solicitacoes_geradas;

        $this->load->view('resultado_solicitacao_view', $dados);
    }

}
