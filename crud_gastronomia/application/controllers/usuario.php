<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class usuario extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('usuario_model', 'model', TRUE);
        $this->load->model('tipo_acesso_model', 'tipo_model', TRUE);
        $this->load->library('session');
        $this->load->library('validacao');
    }

    function index() {
//        if ($this->validacao->check_session() == 1) {
            $this->load->helper('form');

//            $session_data = $this->session->userdata('logged_in');

            $data['titulo'] = "Cadastro de Usuário";
            $data['usuario'] = $this->model->listar();
            $data['tipo_acesso'] = $this->tipo_model->listar();
            $this->load->view('usuario_view.php', $data);
//        } else {
//            redirect('verifica_login', 'refresh');
//        }
    }

    function getCamposCadastrados() {
        $objetos_cadastrados = $this->model->listar();

        $arr_campos_cadastrados = array();
        foreach ($objetos_cadastrados as $obj) {
            $arr_campos_cadastrados[] = '{"campo_validado":"' . $obj->matricula . '"}';
        }
        echo '[ ' . implode(",", $arr_campos_cadastrados) . ']';

        return;
    }

    function getCampoEditado($id_obj) {
        $objeto = $this->model->editar($id_obj);
        echo '[{"id":' . $objeto[0]->id_usuario . ',"campo_validado":"' . $objeto[0]->matricula . '"}]';
        return;
    }

    function inserir() {
        /* Recebe os dados do formulário (visão) */
        $data['nome_usuario'] = $this->input->post('nome_usuario');
        $data['matricula'] = $this->input->post('matricula');
        $data['senha'] = md5($this->input->post('senha'));
        $data['email'] = $this->input->post('email');
        $data['usuario_id_tipo_acesso'] = $this->input->post('usuario_id_tipo_acesso');

        /* Chama a função inserir do modelo */
        if ($this->model->inserir($data)) {
            redirect('usuario');
        } else {
            log_message('error', 'Erro ao inserir o usuário.');
        }
    }

    function editar($id_usuario) {
        /* Aqui vamos definir o título da página de edição */
        $data['titulo'] = "Editar Usuários";

        $data['tipo_acesso'] = $this->tipo_model->listar();
        /* Busca os dados da pessoa que será editada */
        $data['dados_usuario'] = $this->model->editar($id_usuario);

        /* Carrega a página de edição com os dados da pessoa */
        $this->load->view('usuario_edit', $data);
    }

    function atualizar() {
        /* Senão obtém os dados do formulário */
        $data['id_usuario'] = $this->input->post('id_usuario');
        $data['nome_usuario'] = $this->input->post('nome_usuario');
        $data['matricula'] = $this->input->post('matricula');
        $data['senha'] = md5($this->input->post('senha'));
        $data['email'] = $this->input->post('email');
        $data['usuario_id_tipo_acesso'] = $this->input->post('usuario_id_tipo_acesso');

        /* Executa a função atualizar do modelo passando como parâmetro os dados obtidos do formulário */
        if ($this->model->atualizar($data)) {
            redirect('usuario');
        } else {
            log_message('error', 'Erro ao inserir o usuário.');
        }
    }

    function deletar($id_usuario) {
        /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
        if ($this->model->deletar($id_usuario)) {
            redirect('usuario');
        } else {
            log_message('error', 'Erro ao deletar o usuário.');
        }
    }

}
