<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class solicitacoes_pendentes extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('solicitacao_model', 'model', TRUE);
        $this->load->model('insumo_model', 'ins_model', TRUE);
        $this->load->model('fornecedor_model', 'for_model', TRUE);
        $this->load->model('solicitacao_insumo_model', 'sol_ins_model', TRUE);
        $this->load->library('session');
        $this->load->library('validacao');
    }

    function index() {
        if ($this->validacao->check_session() == 1) {
            $this->load->helper('form');

            $session_data = $this->session->userdata('logged_in');

            $data['titulo'] = "Solicitações Pendentes";
            $data['solicitacao'] = $this->model->listar();
            $data['insumo'] = $this->ins_model->listar();
            $data['fornecedor'] = $this->for_model->listar();
            $data['solicitacao_insumo'] = $this->sol_ins_model->listar();
            $this->load->view('solicitacoes_pendentes_view.php', $data);
        } else {
            redirect('verifica_login', 'refresh');
        }
    }

}
