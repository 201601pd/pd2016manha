<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class roteiro extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('roteiro_model', 'model', TRUE);
        $this->load->model('curso_model', 'cur_model', TRUE);
        $this->load->library('session');
        $this->load->library('validacao');
    }

    function index() {
         if ($this->validacao->check_session() == 1 || $this->validacao->check_session() == 2) {
            $this->load->helper('form');

            $session_data = $this->session->userdata('logged_in');
            
            $data['titulo'] = "Cadastro de Cursos";
            $data['roteiro'] = $this->model->listar();
            $data['curso'] = $this->cur_model->listar();
            $this->load->view('roteiro_view.php', $data);
        } else {
            redirect('verifica_login', 'refresh');
        }
    }

    function inserir() {
        /* Recebe os dados do formulário (visão) */
        $data['titulo'] = $this->input->post('titulo');
        $data['roteiro_id_curso'] = $this->input->post('roteiro_id_curso');

        /* Chama a função inserir do modelo */
        if ($this->model->inserir($data)) {
            redirect('roteiro');
        } else {
            log_message('error', 'Erro ao inserir o roteiro.');
        }
    }

    function editar($id_roteiro) {
        /* Aqui vamos definir o título da página de edição */
        $data['titulo'] = "Editar Roteiros | AppGastronomia";

        $data['curso'] = $this->cur_model->listar();
        /* Busca os dados da pessoa que será editada */
        $data['dados_roteiro'] = $this->model->editar($id_roteiro);

        /* Carrega a página de edição com os dados da pessoa */
        $this->load->view('roteiro_edit', $data);
    }

    function atualizar() {
        /* Senão obtém os dados do formulário */
        $data['id_roteiro'] = $this->input->post('id_roteiro');
        $data['titulo'] = $this->input->post('titulo');
        $data['roteiro_id_curso'] = $this->input->post('roteiro_id_curso');

        /* Executa a função atualizar do modelo passando como parâmetro os dados obtidos do formulário */
        if ($this->model->atualizar($data)) {
            redirect('roteiro');
        } else {
            log_message('error', 'Erro ao inserir o roteiro.');
        }
    }

    function deletar($id_roteiro) {
        /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
        if ($this->model->deletar($id_roteiro)) {
            redirect('roteiro');
        } else {
            log_message('error', 'Erro ao deletar o roteiro.');
        }
    }

}
