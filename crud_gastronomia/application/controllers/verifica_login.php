<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class verifica_login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('usuario_model', '', TRUE);
    }

    function index() {
        //This method will have the credentials validation
        $this->load->library('form_validation');

        $this->form_validation->set_rules('matricula', 'Matricula', 'trim|required');
        $this->form_validation->set_rules('senha', 'Senha', 'trim|required|callback_check_database');

        if ($this->form_validation->run() == FALSE) {
            //Field validation failed.  User redirected to login page
            $data['titulo'] = "Autenticação";
            $this->load->view('index_login', $data);
        } else {
            //Go to private area
            redirect('dashboard', 'refresh');
        }
    }

    function check_database($senha) {
        //Field validation succeeded.  Validate against database
        $matricula = $this->input->post('matricula');

        //query the database
        $result = $this->usuario_model->login($matricula, $senha);

        if ($result) {
            $sess_array = array();
            foreach ($result as $row) {
                $sess_array = array(
                    'session_id' => session_id(),
                    'id_usuario' => $row->id_usuario,
                    'nome_usuario' => $row->nome_usuario,
                    'matricula' => $row->matricula,
                    'tipo_usuario' => $row->usuario_id_tipo_acesso
                );
                $this->session->set_userdata('logged_in', $sess_array);
            }
            return TRUE;
        } else {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }

}
