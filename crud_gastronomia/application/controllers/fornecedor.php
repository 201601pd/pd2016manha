<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class fornecedor extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('fornecedor_model', 'model', TRUE);
        $this->load->library('session');
        $this->load->library('validacao');
    }

    function index() {
        if ($this->validacao->check_session() == 1) {
            $this->load->helper('form');

            $session_data = $this->session->userdata('logged_in');
            
            $data['titulo'] = "Cadastro de Fornecedores";
            $data['fornecedor'] = $this->model->listar();
            $this->load->view('fornecedor_view.php', $data);
        } else {
            redirect('verifica_login', 'refresh');
        }
    }

    function getCamposCadastrados() {
        $objetos_cadastrados = $this->model->listar();

        $arr_campos_cadastrados = array();
        foreach ($objetos_cadastrados as $obj) {
            $arr_campos_cadastrados[] = '{"campo_validado":"' . $obj->cnpj . '"}';
        }
        echo '[ ' . implode(",", $arr_campos_cadastrados) . ']';

        return;
    }

    function getCampoEditado($id_obj) {
        $objeto = $this->model->editar($id_obj);
        echo '[{"id":' . $objeto[0]->id_fornecedor . ',"campo_validado":"' . $objeto[0]->cnpj . '"}]';
        return;
    }

    function inserir() {
        /* Recebe os dados do formulário (visão) */
        $data['cnpj'] = $this->input->post('cnpj');
        $data['status_fornecedor'] = $this->input->post('status_fornecedor');
        $data['razao_social'] = $this->input->post('razao_social');
        $data['endereco'] = $this->input->post('endereco');
        $data['telefone'] = $this->input->post('telefone');
        $data['email'] = $this->input->post('email');

        /* Chama a função inserir do modelo */
        if ($this->model->inserir($data)) {
            redirect('fornecedor');
        } else {
            log_message('error', 'Erro ao inserir o fornecedor.');
        }
    }

    function editar($id_fornecedor) {
        /* Aqui vamos definir o título da página de edição */
        $data['titulo'] = "Editar Fornecedor | AppGastronomia";

        /* Busca os dados da pessoa que será editada */
        $data['dados_fornecedor'] = $this->model->editar($id_fornecedor);

        /* Carrega a página de edição com os dados da pessoa */
        $this->load->view('fornecedor_edit', $data);
    }

    function atualizar() {
        $data['id_fornecedor'] = $this->input->post('id_fornecedor');
        $data['cnpj'] = $this->input->post('cnpj');
        $data['status_fornecedor'] = $this->input->post('status_fornecedor');
        $data['razao_social'] = $this->input->post('razao_social');
        $data['endereco'] = $this->input->post('endereco');
        $data['telefone'] = $this->input->post('telefone');
        $data['email'] = $this->input->post('email');

        /* Executa a função atualizar do modelo passando como parâmetro os dados obtidos do formulário */
        if ($this->model->atualizar($data)) {
            redirect('fornecedor');
        } else {
            log_message('error', 'Erro ao atualizar o fornecedor.');
        }
    }

    function deletar($id_fornecedor) {
        /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
        if ($this->model->deletar($id_fornecedor)) {
            redirect('fornecedor');
        } else {
            log_message('error', 'Erro ao deletar o fornecedor.');
        }
    }

}
