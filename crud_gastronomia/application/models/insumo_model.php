<?php

class insumo_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('insumo', $data);
    }

    function listar() {
        $query = $this->db->get('insumo');
        return $query->result();
    }

    function editar($id_insumo) {
        $this->db->where('id_insumo', $id_insumo);
        $query = $this->db->get('insumo');
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where('id_insumo', $data['id_insumo']);
        $this->db->set($data);
        return $this->db->update('insumo');
    }

    function deletar($id_insumo) {
        $this->db->where('id_insumo', $id_insumo);
        return $this->db->delete('insumo');
    }

}
