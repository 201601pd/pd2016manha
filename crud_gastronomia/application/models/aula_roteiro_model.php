<?php

class aula_roteiro_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('aula_roteiro', $data);
    }

    function listar() {
        $query = $this->db->get('aula_roteiro');
        return $query->result();
    }

    function editar($url) {
        list($aula_roteiro_id_aula, $aula_roteiro_id_roteiro) = explode("-", $url);
        $this->db->where('aula_roteiro_id_aula', $aula_roteiro_id_aula);
        $this->db->where('aula_roteiro_id_roteiro', $aula_roteiro_id_roteiro);
        $query = $this->db->get('aula_roteiro');
        return $query->result();
    }

    function atualizar($data, $auxData) {
        $this->db->where('aula_roteiro_id_aula', $auxData['old_aula_roteiro_id_aula']);
        $this->db->where('aula_roteiro_id_roteiro', $auxData['old_aula_roteiro_id_roteiro']);
        $this->db->set($data);
        return $this->db->update('aula_roteiro');
    }

    function deletar($url) {
        list($aula_roteiro_id_aula, $aula_roteiro_id_roteiro) = explode("-", $url);
        $this->db->where('aula_roteiro_id_aula', $aula_roteiro_id_aula);
        $this->db->where('aula_roteiro_id_roteiro', $aula_roteiro_id_roteiro);
        return $this->db->delete('aula_roteiro');
    }

}
