<?php

class aula_receita_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('aula_receita', $data);
    }

    function listar() {
        $query = $this->db->get('aula_receita');
        return $query->result();
    }

    function editar($url) {
        list($aula_receita_id_aula, $aula_receita_id_receita) = explode("-", $url);
        $this->db->where('aula_receita_id_aula', $aula_receita_id_aula);
        $this->db->where('aula_receita_id_receita', $aula_receita_id_receita);
        $query = $this->db->get('aula_receita');
        return $query->result();
    }

    function atualizar($data, $auxData) {
        $this->db->where('aula_receita_id_aula', $auxData['old_aula_receita_id_aula']);
        $this->db->where('aula_receita_id_receita', $auxData['old_aula_receita_id_receita']);
        $this->db->set($data);
        return $this->db->update('aula_receita');
    }

    function deletar($url) {
        list($aula_receita_id_aula, $aula_receita_id_receita) = explode("-", $url);
        $this->db->where('aula_receita_id_aula', $aula_receita_id_aula);
        $this->db->where('aula_receita_id_receita', $aula_receita_id_receita);
        return $this->db->delete('aula_receita');
    }

}
