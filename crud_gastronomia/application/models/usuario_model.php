<?php

class usuario_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('usuario', $data);
    }

    function listar() {
        $query = $this->db->get('usuario');
        return $query->result();
    }

    function editar($id_usuario) {
        $this->db->where('id_usuario', $id_usuario);
        $query = $this->db->get('usuario');
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where('id_usuario', $data['id_usuario']);
        $this->db->set($data);
        return $this->db->update('usuario');
    }

    function deletar($id_usuario) {
        $this->db->where('id_usuario', $id_usuario);
        return $this->db->delete('usuario');
    }
    
    function login($matricula, $senha){
        $this->db->select('id_usuario, matricula, senha, usuario_id_tipo_acesso, nome_usuario');
        $this->db->from('usuario');
        $this->db->where('matricula', $matricula);
        $this->db->where('senha', md5($senha));
        $this->db->limit(1);
        
        $query = $this->db->get();
        
        if($query -> num_rows() == 1){
            return $query->result();
        }else{
            return false;
        }
        
    }//fim da function

}
