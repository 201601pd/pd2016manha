<?php

class categoria_receita_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('categoria_receita', $data);
    }

    function listar() {
        $query = $this->db->get('categoria_receita');
        return $query->result();
    }

    function editar($id_categoria_receita) {
        $this->db->where('id_categoria_receita', $id_categoria_receita);
        $query = $this->db->get('categoria_receita');
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where('id_categoria_receita', $data['id_categoria_receita']);
        $this->db->set($data);
        return $this->db->update('categoria_receita');
    }

    function deletar($id_categoria_receita) {
        $this->db->where('id_categoria_receita', $id_categoria_receita);
        return $this->db->delete('categoria_receita');
    }
}
