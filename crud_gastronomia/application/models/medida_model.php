<?php

class medida_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('medida', $data);
    }

    function listar() {
        $query = $this->db->get('medida');
        return $query->result();
    }

    function editar($id_medida) {
        $this->db->where('id_medida', $id_medida);
        $query = $this->db->get('medida');
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where('id_medida', $data['id_medida']);
        $this->db->set($data);
        return $this->db->update('medida');
    }

    function deletar($id_medida) {
        $this->db->where('id_medida', $id_medida);
        return $this->db->delete('medida');
    }

}
