<?php

class categoria_insumo_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('categoria_insumo', $data);
    }

    function listar() {
        $query = $this->db->get('categoria_insumo');
        return $query->result();
    }

    function editar($id_categoria_insumo) {
        $this->db->where('id_categoria_insumo', $id_categoria_insumo);
        $query = $this->db->get('categoria_insumo');
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where('id_categoria_insumo', $data['id_categoria_insumo']);
        $this->db->set($data);
        return $this->db->update('categoria_insumo');
    }

    function deletar($id_categoria_insumo) {
        $this->db->where('id_categoria_insumo', $id_categoria_insumo);
        return $this->db->delete('categoria_insumo');
    }
}
