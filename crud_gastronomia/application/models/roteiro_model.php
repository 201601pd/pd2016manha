<?php

class roteiro_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('roteiro', $data);
    }

    function listar() {
        $query = $this->db->get('roteiro');
        return $query->result();
    }

    function editar($id_roteiro) {
        $this->db->where('id_roteiro', $id_roteiro);
        $query = $this->db->get('roteiro');
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where('id_roteiro', $data['id_roteiro']);
        $this->db->set($data);
        return $this->db->update('roteiro');
    }

    function deletar($id_roteiro) {
        $this->db->where('id_roteiro', $id_roteiro);
        return $this->db->delete('roteiro');
    }

    function getRoteirosById($id = null) {
        if (!is_null($id))
            $this->db->where(array('curso.id_curso' => $id));
        return $this->db->select('roteiro.id_roteiro, roteiro.titulo')
                        ->from('curso')
                        ->join('roteiro', 'roteiro.roteiro_id_curso = curso.id_curso')
                        ->get()->result();
    }
}
