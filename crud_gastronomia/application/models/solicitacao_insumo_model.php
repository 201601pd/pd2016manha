<?php

class solicitacao_insumo_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('solicitacao_insumo', $data);
    }

    function inserir_batch($data) {
        return $this->db->insert_batch('solicitacao_insumo', $data);
    }

    function listar() {
        $query = $this->db->get('solicitacao_insumo');
        return $query->result();
    }

    function editar($url) {
        list($solicitacao_insumo_id_insumo, $solicitacao_insumo_id_solicitacao) = explode("-", $url);
        $this->db->where('solicitacao_insumo_id_insumo', $solicitacao_insumo_id_insumo);
        $this->db->where('solicitacao_insumo_id_solicitacao', $solicitacao_insumo_id_solicitacao);
        $query = $this->db->get('solicitacao_insumo');
        return $query->result();
    }

    function atualizar($data, $auxData) {
        $this->db->where('solicitacao_insumo_id_insumo', $auxData['old_solicitacao_insumo_id_insumo']);
        $this->db->where('solicitacao_insumo_id_solicitacao', $auxData['old_solicitacao_insumo_id_solicitacao']);
        $this->db->set($data);
        return $this->db->update('solicitacao_insumo');
    }

    function deletar($url) {
        list($solicitacao_insumo_id_insumo, $solicitacao_insumo_id_solicitacao) = explode("-", $url);
        $this->db->where('solicitacao_insumo_id_insumo', $solicitacao_insumo_id_insumo);
        $this->db->where('solicitacao_insumo_id_solicitacao', $solicitacao_insumo_id_solicitacao);
        return $this->db->delete('solicitacao_insumo');
    }

    function retornaInsumos($id_solicitacao) {
        $query = $this->db->query("SELECT insumo.nome_insumo, fornecedor.razao_social, solicitacao_insumo.quantidade, solicitacao_insumo.observacao "
                . "FROM solicitacao_insumo "
                . "INNER JOIN insumo "
                . "ON solicitacao_insumo.solicitacao_insumo_id_insumo = insumo.id_insumo "
                . "INNER JOIN fornecedor "
                . "ON insumo.insumo_id_fornecedor = fornecedor.id_fornecedor "
                . "WHERE solicitacao_insumo.solicitacao_insumo_id_solicitacao = " . $id_solicitacao);
        return $query->result();
    }

}
