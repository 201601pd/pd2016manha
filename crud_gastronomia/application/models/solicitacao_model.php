<?php

class solicitacao_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('solicitacao', $data);
    }

    function listar() {
        $query = $this->db->get('solicitacao');
        return $query->result();
    }

    function editar($id_solicitacao) {
        $this->db->where('id_solicitacao', $id_solicitacao);
        $query = $this->db->get('solicitacao');
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where('id_solicitacao', $data['id_solicitacao']);
        $this->db->set($data);
        return $this->db->update('solicitacao');
    }

    function deletar($id_solicitacao) {
        $this->db->where('id_solicitacao', $id_solicitacao);
        return $this->db->delete('solicitacao');
    }

    function getNextId() {
        $query = $this->db->query("SELECT `auto_increment` "
                . "FROM INFORMATION_SCHEMA.TABLES "
                . "WHERE table_name = 'solicitacao' ");
        return $query->result();
    }

    function getInsumosByAulaId($aulas_id) {
        $query = $this->db->query('SELECT insumo_receita.insumo_receita_id_insumo '
                . 'AS id_insumo, insumo.nome_insumo as nome_insumo, insumo.qtd_atual, medida.nome_medida, '
                . 'insumo.insumo_id_fornecedor AS insumo_id_fornecedor, '
                . 'SUM(insumo_receita.quantidade) AS qtd_total '
                . 'FROM insumo_receita '
                . 'INNER JOIN insumo '
                . 'ON insumo.id_insumo = insumo_receita.insumo_receita_id_insumo '
                . 'INNER JOIN medida ON insumo.insumo_id_medida = medida.id_medida '
                . 'WHERE insumo_receita.insumo_receita_id_receita '
                . 'IN (SELECT aula_receita.aula_receita_id_receita AS id_receita '
                . 'FROM aula_receita '
                . 'WHERE aula_receita.aula_receita_id_aula IN (' . str_replace("-", ",", $aulas_id) . ')) '
                . 'GROUP BY id_insumo');
        return $query->result();
    }

}
