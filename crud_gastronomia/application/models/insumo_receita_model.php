<?php

class insumo_receita_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('insumo_receita', $data);
    }
    
    function inserir_batch($data) {
        return $this->db->insert_batch('insumo_receita', $data);
    }

    function listar() {
        $query = $this->db->get('insumo_receita');
        return $query->result();
    }

    function editar($url) {
        list($insumo_receita_id_insumo, $insumo_receita_id_receita) = explode("-", $url);
        $this->db->where('insumo_receita_id_insumo', $insumo_receita_id_insumo);
        $this->db->where('insumo_receita_id_receita', $insumo_receita_id_receita);
        $query = $this->db->get('insumo_receita');
        return $query->result();
    }

    function atualizar($data, $auxData) {
        $this->db->where('insumo_receita_id_insumo', $auxData['old_insumo_receita_id_insumo']);
        $this->db->where('insumo_receita_id_receita', $auxData['old_insumo_receita_id_receita']);
        $this->db->set($data);
        return $this->db->update('insumo_receita');
    }

    function deletar($url) {
        list($insumo_receita_id_insumo, $insumo_receita_id_receita) = explode("-", $url);
        $this->db->where('insumo_receita_id_insumo', $insumo_receita_id_insumo);
        $this->db->where('insumo_receita_id_receita', $insumo_receita_id_receita);
        return $this->db->delete('insumo_receita');
    }

}
