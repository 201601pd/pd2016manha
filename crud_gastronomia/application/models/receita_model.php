<?php

class receita_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('receita', $data);
    }

    function listar() {
        $query = $this->db->get('receita');
        return $query->result();
    }

    function editar($id_receita) {
        $this->db->where('id_receita', $id_receita);
        $query = $this->db->get('receita');
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where('id_receita', $data['id_receita']);
        $this->db->set($data);
        return $this->db->update('receita');
    }

    function deletar($id_receita) {
        $this->db->where('id_receita', $id_receita);
        return $this->db->deletar('receita');
    }

    function getNextId() {
        $query = $this->db->query("SELECT `auto_increment` "
                . "FROM INFORMATION_SCHEMA.TABLES "
                . "WHERE table_name = 'receita' ");
        return $query->result();
    }

    function getInsumosRelacionados($id_receita) {
        $query = $this->db->query("SELECT insumo.id_insumo, insumo_receita.insumo_receita_id_receita, insumo.nome_insumo, insumo_receita.quantidade, medida.nome_medida "
                . "FROM insumo "
                . "JOIN insumo_receita "
                . "ON insumo.id_insumo = insumo_receita.insumo_receita_id_insumo "
                . "JOIN medida "
                . "ON insumo.insumo_id_medida = medida.id_medida "
                . "WHERE insumo_receita.insumo_receita_id_receita = " . $id_receita);
        return $query->result();
    }

    function atualizarInsumosRelacionados($novaQuantidade, $idInsumo, $idReceita) {
        return $query = $this->db->query("UPDATE insumo_receita "
                . "SET insumo_receita.quantidade = " . $novaQuantidade . " "
                . "WHERE insumo_receita.insumo_receita_id_insumo = " . $idInsumo . " "
                . "AND insumo_receita.insumo_receita_id_receita = " . $idReceita . " ");
    }

}
