<?php

class tipo_acesso_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('tipo_acesso', $data);
    }

    function listar() {
        $query = $this->db->get('tipo_acesso');
        return $query->result();
    }

    function editar($id_tipo_acesso) {
        $this->db->where('id_tipo_acesso', $id_tipo_acesso);
        $query = $this->db->get('tipo_acesso');
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where('id_tipo_acesso', $data['id_tipo_acesso']);
        $this->db->set($data);
        return $this->db->update('tipo_acesso');
    }

    function deletar($id_tipo_acesso) {
        $this->db->where('id_tipo_acesso', $id_tipo_acesso);
        return $this->db->delete('tipo_acesso');
    }

}
