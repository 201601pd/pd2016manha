<?php

class Fornecedor_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('fornecedor', $data);
    }

    function listar() {
        $query = $this->db->get('fornecedor');
        return $query->result();
    }

    function listarAtivos() {
        $this->db->where('status_fornecedor', 1);
        $query = $this->db->get('fornecedor');
        return $query->result();
    }

    function editar($id_fornecedor) {
        $this->db->where('id_fornecedor', $id_fornecedor);
        $query = $this->db->get('fornecedor');
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where('id_fornecedor', $data['id_fornecedor']);
        $this->db->set($data);
        return $this->db->update('fornecedor');
    }

    function deletar($id_fornecedor) {
        $this->db->where('id_fornecedor', $id_fornecedor);
        return $this->db->delete('fornecedor');
    }

}
