<?php

class aula_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('aula', $data);
    }

    function listar() {
        $query = $this->db->get('aula');
        return $query->result();
    }

    function editar($id_aula) {
        $this->db->where('id_aula', $id_aula);
        $query = $this->db->get('aula');
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where('id_aula', $data['id_aula']);
        $this->db->set($data);
        return $this->db->update('aula');
    }

    function deletar($id_aula) {
        $this->db->where('id_aula', $id_aula);
        return $this->db->delete('aula');
    }

    function getAulasById($id = null) {
        if (!is_null($id))
            $this->db->where(array('roteiro.id_roteiro' => $id));
        $this->db->select('aula.id_aula, aula.nome_aula');
        $this->db->from('aula_roteiro');
        $this->db->join('aula', 'aula.id_aula = aula_roteiro.aula_roteiro_id_aula');
        $this->db->join('roteiro', 'roteiro.id_roteiro = aula_roteiro.aula_roteiro_id_roteiro');
        return $this->db->get()->result();
    }

}
