<?php

class curso_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('curso', $data);
    }

    function listar() {
        $query = $this->db->get('curso');
        return $query->result();
    }

    function editar($id_curso) {
        $this->db->where('id_curso', $id_curso);
        $query = $this->db->get('curso');
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where('id_curso', $data['id_curso']);
        $this->db->set($data);
        return $this->db->update('curso');
    }

    function deletar($id_curso) {
        $this->db->where('id_curso', $id_curso);
        return $this->db->delete('curso');
    }
}
