<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Validacao {
    
    public function check_session() {
        
        $ci = & get_instance();
        //print_r($ci);
        
        if (isset($ci->session->userdata('logged_in')["session_id"]) && 
                 ($ci->session->userdata('logged_in')["tipo_usuario"] == 1)) {
            return 1;
        } else if (isset($ci->session->userdata('logged_in')["session_id"]) && 
                 ($ci->session->userdata('logged_in')["tipo_usuario"] == 2)) {
            return 2;
        } else {
            return 0;
        }
    }
}
 ?>
