﻿-- 1) Serão emitidos alertas caso um insumo esteja próximo ou tenha passado de sua data de validade. *funcional 

SELECT * FROM insumo INNER JOIN entrada ON (insumo.id_insumo = entrada.entrada_id_insumo)
WHERE (entrada.validade < NOW()) OR (entrada.validade BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 10 DAY))

-- 2) Serão emitidos alertas caso um insumo chegue a sua quantidade mínima ou proximo; *funcional

SELECT * FROM insumo WHERE (qtd_minima >= qtd_atual) OR (qtd_atual-qtd_minima <= 10)

-- 3) No intervalo de um mês insumos vencidos e sua quantidade *funcional

SELECT nome_insumo, validade, qtd_atual FROM insumo INNER JOIN entrada ON (insumo.id_insumo = entrada.entrada_id_insumo)
WHERE (entrada.validade < NOW()) AND (entrada.validade BETWEEN DATE_SUB(NOW(), INTERVAL 31 DAY) AND NOW())

-- 4) O sistema deve emitir alerta de solicitações com atraso *funcional

SELECT * FROM solicitacao WHERE (solicitacao.data_requisicao < NOW()) 

-- 5) Próximas aulas (semana) *funcional

SELECT * FROM aula INNER JOIN aula_roteiro ON (aula.id_aula = aula_roteiro.aula_roteiro_id_aula)
WHERE (DAY(aula_roteiro.data_aula_roteiro) <= DAY(DATE_ADD(CURDATE(),INTERVAL 7 DAY)))

-- 6) Solicitações em aberto (data futura de entrega) *funcional

SELECT * FROM solicitacao WHERE (solicitacao.data_requisicao > NOW())


